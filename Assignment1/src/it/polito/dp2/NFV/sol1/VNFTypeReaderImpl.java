package it.polito.dp2.NFV.sol1;

import it.polito.dp2.NFV.FunctionalType;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.sol1.jaxb.VNFType;

class VNFTypeReaderImpl implements VNFTypeReader {
	
	private VNFType vt;
	
	protected VNFTypeReaderImpl(VNFType vt) { 
		this.vt = vt;		
	}

	@Override
	public String getName() {
		return vt.getName();
	}

	@Override
	public FunctionalType getFunctionalType() {
		return FunctionalType.fromValue(vt.getType().name());
	}

	@Override
	public int getRequiredMemory() { return vt.getMemoryMB(); }

	@Override
	public int getRequiredStorage() { return vt.getStorageMB(); }

}
