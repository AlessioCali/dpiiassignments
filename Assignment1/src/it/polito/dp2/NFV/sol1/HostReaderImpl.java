package it.polito.dp2.NFV.sol1;

import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.sol1.jaxb.HostType;

class HostReaderImpl implements HostReader {

	private NfvReaderImpl nfv;
	private HostType ht;
	
	protected HostReaderImpl(HostType ht, NfvReaderImpl nfv) {
		this.ht = ht;
		this.nfv = nfv;
	}
	
	@Override
	public String getName() { return ht.getName(); }

	@Override
	public int getAvailableMemory() { return ht.getMemoryMB(); }

	@Override
	public int getAvailableStorage() { return ht.getStorageMB(); }

	@Override
	public int getMaxVNFs() { return ht.getMaxVNF(); }

	@Override
	public Set<NodeReader> getNodes() {
		return ht.getAllocations()
				 .getNodeAllocation()
				 .stream()
				 .filter(nat -> nfv.getNode(nat.getNode()) != null)
				 .map(nat -> new NodeReaderImpl(nfv.getNode(nat.getNode()), nat.getNffg(), nfv))
				 .collect(Collectors.toSet());
	}
}
