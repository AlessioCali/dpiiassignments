package it.polito.dp2.NFV.sol1;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFV.ConnectionPerformanceReader;
import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.sol1.jaxb.CPType;
import it.polito.dp2.NFV.sol1.jaxb.HostType;
import it.polito.dp2.NFV.sol1.jaxb.INType;
import it.polito.dp2.NFV.sol1.jaxb.NFFGType;
import it.polito.dp2.NFV.sol1.jaxb.NodeType;

class NfvReaderImpl implements NfvReader {
	
	private INType root;
	
	private HashMap<String, NodeType> nodeMap = new HashMap<>();
	
	protected NfvReaderImpl(INType root) {
		this.root = root;
		
		// Build auxiliary map for nodes. This avoids doing an NFFG->Node lookup
		for (NFFGType nffg : root.getNFFGs().getNFFG()) {
			for (NodeType nt : nffg.getNode()) {
				nodeMap.put(nt.getName(), nt);
			}
		}
	}

	@Override
	public ConnectionPerformanceReader getConnectionPerformance(HostReader hr0, HostReader hr1) {
		HostType source =
				root.getHosts().getHost().stream()
										 .filter( ht -> ht.getName().equals(hr0.getName()) )
										 .findFirst()
										 .orElse(null);
		
		if (source == null) { return null; }
		else {
			CPType conn = source.getConnections().getConnection()
												  .stream()
												  .filter(cp -> cp.getTo().equals(hr1.getName()))
												  .findFirst()
												  .orElse(null);
			
			if (conn == null) { return null; }
			else { return new ConnectionPerformanceImpl(conn); }
		}
	}

	@Override
	public HostReader getHost(String hostName) {
		HostType ht = root.getHosts().getHost().stream()
											   .filter(h -> h.getName().equals(hostName))
											   .findFirst()
											   .orElse(null);
		if (ht != null) { return new HostReaderImpl(ht, this); }
		else { return null; }
	}

	@Override
	public Set<HostReader> getHosts() {
		return root.getHosts().getHost().stream()
										.map(host -> new HostReaderImpl(host, this))
										.collect(Collectors.toSet());
	}

	@Override
	public NffgReader getNffg(String nffgName) {
		NFFGType nft = root.getNFFGs().getNFFG().stream()
												.filter(nffg -> nffg.getName().equals(nffgName))
												.findFirst()
												.orElse(null);
		if (nft != null) { return new NffgReaderImpl(nft, this); }
		else { return null; }
	}

	@Override
	public Set<NffgReader> getNffgs(Calendar since) {
		return root.getNFFGs().getNFFG().stream()
										.filter(nffg -> since == null || nffg.getDeployTime().toGregorianCalendar().compareTo(since) >= 0)
										.map(nffg -> new NffgReaderImpl(nffg, this))
										.collect(Collectors.toSet());
	}

	@Override
	public Set<VNFTypeReader> getVNFCatalog() {
		return root.getVNFCatalog().getVNF().stream()
											.map(vnf -> new VNFTypeReaderImpl(vnf))
											.collect(Collectors.toSet());
	}
	
	protected NodeType getNode(String nodeName) {
		return nodeMap.get(nodeName);
	}
}
