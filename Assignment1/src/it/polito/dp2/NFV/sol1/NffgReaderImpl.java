package it.polito.dp2.NFV.sol1;

import java.util.Calendar;
import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.sol1.jaxb.NFFGType;

class NffgReaderImpl implements NffgReader {

	private NFFGType nft;
	private NfvReaderImpl nfv;
	
	protected NffgReaderImpl(NFFGType nft, NfvReaderImpl nfv) {
		this.nft = nft;
		this.nfv = nfv;
	}
	
	@Override
	public String getName() { return nft.getName(); }

	@Override
	public Calendar getDeployTime() { return nft.getDeployTime().toGregorianCalendar(); }

	@Override
	public NodeReader getNode(String nodeName) {
		return nft.getNode().stream()
							.filter(nt -> nt.getName().equals(nodeName))
							.map(nt -> new NodeReaderImpl(nt, nft.getName(), nfv))
							.findFirst()
							.orElse(null);
	}

	@Override
	public Set<NodeReader> getNodes() {
		return nft.getNode().stream()
							.map(nt -> new NodeReaderImpl(nt, nft.getName(), nfv))
							.collect(Collectors.toSet());
	}

}
