package it.polito.dp2.NFV.sol1;

import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.sol1.jaxb.LinkType;
import it.polito.dp2.NFV.sol1.jaxb.NodeType;

class LinkReaderImpl implements LinkReader {

	private NodeReaderImpl sourceNodeReader;
	private NfvReaderImpl nfv;
	private LinkType lt;
	
	protected LinkReaderImpl(LinkType lt, NfvReaderImpl nfv, NodeReaderImpl sourceNodeReader) {
		this.nfv = nfv;
		this.lt = lt;
		this.sourceNodeReader = sourceNodeReader;
	}
	
	@Override
	public String getName() { return lt.getName(); }

	@Override
	public NodeReader getDestinationNode() { 
		NodeType dest = nfv.getNode(lt.getDest());
		if (dest != null) { return new NodeReaderImpl(dest, sourceNodeReader.getNffgName(), nfv); }
		else { return null; }
	}

	@Override
	public int getLatency() { return lt.getLatency(); }

	@Override
	public NodeReader getSourceNode() {	return sourceNodeReader; }

	@Override
	public float getThroughput() { return lt.getThroughput(); }

}
