package it.polito.dp2.NFV.sol1;

/**
 * This exception is thrown if the source data generates some fatal exception during serialization
 * (e.g., an invalid VNF signature).
 * 
 * @author Alessio Calì
 *
 */
public class DataSerializationException extends Exception {

	private static final long serialVersionUID = -3465422659145850386L;

	public DataSerializationException() {
		super();
	}

	public DataSerializationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DataSerializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataSerializationException(String message) {
		super(message);
	}

	public DataSerializationException(Throwable cause) {
		super(cause);
	}
	
	

}
