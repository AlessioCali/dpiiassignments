package it.polito.dp2.NFV.sol1;

import it.polito.dp2.NFV.ConnectionPerformanceReader;
import it.polito.dp2.NFV.sol1.jaxb.CPType;

class ConnectionPerformanceImpl implements ConnectionPerformanceReader {

	private CPType conn;
	
	protected ConnectionPerformanceImpl(CPType conn) { this.conn = conn; }
	
	@Override
	public int getLatency() { return conn.getLatency(); }

	@Override
	public float getThroughput() { return conn.getThroughput(); }

}
