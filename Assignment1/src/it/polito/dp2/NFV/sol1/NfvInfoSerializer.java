package it.polito.dp2.NFV.sol1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.SchemaFactoryConfigurationError;

import org.xml.sax.SAXException;

import it.polito.dp2.NFV.ConnectionPerformanceReader;
import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NfvReaderException;
import it.polito.dp2.NFV.NfvReaderFactory;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.sol1.jaxb.CPType;
import it.polito.dp2.NFV.sol1.jaxb.HostType;
import it.polito.dp2.NFV.sol1.jaxb.HostType.Allocations;
import it.polito.dp2.NFV.sol1.jaxb.HostType.Connections;
import it.polito.dp2.NFV.sol1.jaxb.INType;
import it.polito.dp2.NFV.sol1.jaxb.INType.Hosts;
import it.polito.dp2.NFV.sol1.jaxb.INType.NFFGs;
import it.polito.dp2.NFV.sol1.jaxb.INType.VNFCatalog;
import it.polito.dp2.NFV.sol1.jaxb.LinkType;
import it.polito.dp2.NFV.sol1.jaxb.NAType;
import it.polito.dp2.NFV.sol1.jaxb.NFFGType;
import it.polito.dp2.NFV.sol1.jaxb.NodeType;
import it.polito.dp2.NFV.sol1.jaxb.ObjectFactory;
import it.polito.dp2.NFV.sol1.jaxb.VNFCode;
import it.polito.dp2.NFV.sol1.jaxb.VNFType;

public class NfvInfoSerializer {
	
	private static final int ECODE_GEN = 1;
	private static final int ECODE_FNF = 3;
	private static final int ECODE_SERIALIZE = 11;
	private static final int ECODE_JAXB = 12;
	
	private final static String NFV_SCHEMA_LOC = "xsd/nfvInfo.xsd";
	
	private NfvReader monitor;
	
	public NfvInfoSerializer() throws NfvReaderException {
		NfvReaderFactory factory = NfvReaderFactory.newInstance();
		monitor = factory.newNfvReader();
	}
	
	public NfvInfoSerializer(NfvReader monitor) {
		this.monitor = monitor;
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("No output file specified.");
			System.exit(0);
		}
		
		try {
			NfvInfoSerializer serializer = new NfvInfoSerializer();
			serializer.convert(args[0]);
		}
		catch (NfvReaderException nie) {
			System.err.println("Error while instantiating generator");
			nie.printStackTrace();
			System.exit(ECODE_GEN);
		}
		catch (FileNotFoundException fnfe) {
			System.err.println("Output file error.");
			fnfe.printStackTrace();
			System.exit(ECODE_FNF);
		}
		catch (DataSerializationException dve) {
			System.err.println("An error occurred while serializing source data");
			dve.printStackTrace();
			System.exit(ECODE_SERIALIZE);
		}
		catch (JAXBException jxbe) {
			System.err.println("An error occurred while performing JAXB operations");
			jxbe.printStackTrace();
			System.exit(ECODE_JAXB);
		}
		
		System.exit(0);
	}
	
	/**
	 * Sets the source reader for this serializer.
	 * 
	 * @param monitor
	 */
	public void setMonitor(NfvReader monitor) {
		this.monitor = monitor;
	}

	/**
	 * Serializes the current monitor and outputs data to the given filename.
	 * 
	 * @param filename the file where to output serialized data.
	 * @throws DataSerializationException if a fatal serialization exception arises.
	 * @throws FileNotFoundException if the output file cannot be opened.
	 * @throws JAXBException If an exception related to JAXB is raised.
	 */
	public void convert(String filename) throws DataSerializationException, FileNotFoundException, JAXBException {
		BufferedOutputStream fos = null;
		BufferedInputStream schemaFile = null;
		
		try {
			// Try to load schema for validation
			Schema s = null;
			try {
				SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
				schemaFile = new BufferedInputStream(new FileInputStream(NFV_SCHEMA_LOC));
				s = sf.newSchema(new StreamSource(schemaFile));			
			}
			catch (FileNotFoundException fnfe) {
				System.err.println("Couldn't locate schema file. Output will not be validated.");
				fnfe.printStackTrace();
				s = null;
			} 
			catch (SAXException e) {
				System.err.println("Error in source schema file. Output will not be validated.");
				e.printStackTrace();
				s = null;
			}
			catch (SchemaFactoryConfigurationError sfce) {
				System.err.println("Error in schema configuration. Output will not be validated.");
				sfce.printStackTrace();
				s = null;
			}
			
			// Prepare output file and JAXB Context
			fos = new BufferedOutputStream(new FileOutputStream(filename));
			JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.NFV.sol1.jaxb");
			JAXBElement<INType> root = serialize(monitor);
			
			// Start marshalling onto output file
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			if (s != null) { m.setSchema(s); }
			m.marshal(root, fos);
		}
		finally {
			// Cleanup file streams
			try { if (fos != null) { fos.close(); } }
			catch (IOException ioe) {
				System.err.println("An exception was raised while closing the output file");
				ioe.printStackTrace();
			}
			try { if (schemaFile != null) { schemaFile.close(); } }
			catch (IOException ioe) {
				System.err.println("An exception was raised while closing the schema file");
				ioe.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Serializes the given source, returning the corresponding root XML elemenent for unmarshalling.
	 * 
	 * @param source The NfvReader whose model serialize.
	 * @return The JAXBElement mirroring the source's structure.
	 * @throws DataSerializationException if an error occurs during serialization.
	 */
	private JAXBElement<INType> serialize(NfvReader source) throws DataSerializationException {
		INType inElement = serializeIFN(source);
		JAXBElement<INType> root = (new ObjectFactory()).createInfrastructureNetwork(inElement);
		return root;
	}
	
	/**
	 * Creates the root INType for this InfrastructureNetwork.
	 * 
	 * @param reader the root NfvReader
	 * @return An INType mirroring the reader.
	 * @throws DataSerializationException If an error occurs during serialization.
	 */
	private INType serializeIFN(NfvReader reader) throws DataSerializationException {
		INType inElement = new INType();
		inElement.setVNFCatalog(serializeCatalog(reader));
		inElement.setHosts(serializeHosts(reader));
		inElement.setNFFGs(serializeNffgs(reader));
		
		return inElement;
	}

	/**
	 * Groups a set of VNFs in a VNFCatalog.
	 * 
	 * @param reader the NfvReader from where to fetch the catalog.
	 * @return The VNFCatalog element mirroring the input set.
	 * @throws DataSerializationException if an invalid VNF name is found.
	 */
	private VNFCatalog serializeCatalog(NfvReader reader) throws DataSerializationException {
		Set<VNFTypeReader> vnfs = reader.getVNFCatalog();
		VNFCatalog catalog = new VNFCatalog();
		for (VNFTypeReader vnf : vnfs) {
			VNFType vnfElement = new VNFType();
			vnfElement.setName(vnf.getName());
			vnfElement.setMemoryMB(vnf.getRequiredMemory());
			vnfElement.setStorageMB(vnf.getRequiredStorage());
			try {
				vnfElement.setType(VNFCode.fromValue(vnf.getFunctionalType().value()));
			}
			catch (IllegalArgumentException iae) {
				throw new DataSerializationException("No VNF for name: " + vnf.getFunctionalType().value(), iae);
			}
			catalog.getVNF().add(vnfElement);
		}
		return catalog;
	}

	/**
	 * Serializes a set of HostReaders in a Hosts element.
	 * 
	 * @param reader the NfvReader from where to get the HostReaders.
	 * @return a serialized Hosts element mirroring the reader's hosts structure. 
	 */
	private Hosts serializeHosts(NfvReader reader) {
		Set<HostReader> hosts = reader.getHosts();
		Hosts hostList = new Hosts();
		for (HostReader hr : hosts) {
			HostType hostElement = new HostType();
			hostElement.setName(hr.getName());
			hostElement.setMaxVNF(hr.getMaxVNFs());
			hostElement.setMemoryMB(hr.getAvailableMemory());
			hostElement.setStorageMB(hr.getAvailableStorage());
			hostElement.setConnections(serializeHostConnections(hr, reader));
			hostElement.setAllocations(serializeHostNodes(hr));
			hostList.getHost().add(hostElement);
		}
		return hostList;
	}

	/**
	 * Serializes node references within a Host.
	 * 
	 * @param hr the HostReader from which to fetch the Node list.
	 * @return a serialized Allocations element with all node references for the given host.
	 */
	private Allocations serializeHostNodes(HostReader hr) {
		Allocations allocations = new Allocations();
		for (NodeReader nr : hr.getNodes()) {
			NAType na = new NAType();
			na.setNode(nr.getName());
			na.setNffg(nr.getNffg().getName());
			allocations.getNodeAllocation().add(na);
		}
		return allocations;
	}

	/**
	 * Serializes all connections starting from a  given host. Said connections are returned as a
	 * {@link Connections} type. 
	 * 
	 * @param source The source host
	 * @param reader The NfvReader from where to fetch connection details.
	 * @return a Connections element for the given source host.
	 */
	private Connections serializeHostConnections(HostReader source, NfvReader reader) {
		Connections cs = new Connections();
		Set<HostReader> hosts = reader.getHosts();
		for (HostReader dest : hosts) {
			CPType cp = new CPType();
			ConnectionPerformanceReader performance = reader.getConnectionPerformance(source, dest);
			if (performance != null) {
				cp.setLatency(performance.getLatency());
				cp.setThroughput(performance.getThroughput());
				cp.setTo(dest.getName());
				cs.getConnection().add(cp);
			}
		}
		return cs;
	}

	/**
	 * Serializes the set of NFFGs from the given NfvReader.
	 * 
	 * @param nffgs The NfvReader from which to fetch the Nffg set.
	 * @return a serialized NFFGs element mirroring the source structure.
	 * @throws DataSerializationException If an error occurs while converting dates.
	 */
	private NFFGs serializeNffgs(NfvReader reader) throws DataSerializationException {
		Set<NffgReader> nffgs = reader.getNffgs(null);
		NFFGs nffgListElement = new NFFGs();
		try {
			DatatypeFactory calendarFactory = DatatypeFactory.newInstance();
			for (NffgReader nffg : nffgs) {
				NFFGType nffgElement = new NFFGType();
				nffgElement.setName(nffg.getName());
				nffgElement.setDeployTime(calendarFactory.newXMLGregorianCalendar((GregorianCalendar)nffg.getDeployTime()));
				serializeNodes(nffgElement, nffg);
				nffgListElement.getNFFG().add(nffgElement);
			}
		}
		catch (DatatypeConfigurationException dce) {
			throw new DataSerializationException("Error while instantiating data type factory", dce);
		}
		catch (ClassCastException cce) {
			throw new DataSerializationException("Error while converting calendar.", cce);
		}	
		return nffgListElement;
	}

	/**
	 * Serializes a set of nodes belonging to a given Nffg.
	 * 
	 * @param nffgType The serialized Nffg to which add the nodes.
	 * @param nffg The source Nffg structure.
	 * @throws DataSerializationException if any node bears an invalid VNF name.
	 */
	private void serializeNodes(NFFGType nffgType, NffgReader nffg) throws DataSerializationException {
		for (NodeReader nr : nffg.getNodes()) {
			NodeType nodeElement = new NodeType();
			nodeElement.setName(nr.getName());
			nodeElement.setHost(nr.getHost().getName());
			try {
				nodeElement.setVnf(nr.getFuncType().getName());				
			}
			catch (IllegalArgumentException iae) {
				throw new DataSerializationException("No VNF for name: " + nr.getFuncType().getFunctionalType().value(), iae);
			}
			serializeLinks(nodeElement, nr.getLinks());
			nffgType.getNode().add(nodeElement);
		}
	}

	/**
	 * Serializes a set of links starting at a given node.
	 * 
	 * @param nt the source serialized node.
	 * @param links the set of links to serialize and add.
	 */
	private void serializeLinks(NodeType nt, Set<LinkReader> links) {
		for (LinkReader lr : links) {
			LinkType linkElement = new LinkType();
			linkElement.setName(lr.getName());
			linkElement.setDest(lr.getDestinationNode().getName());
			linkElement.setLatency(lr.getLatency());
			linkElement.setThroughput(lr.getThroughput());
			nt.getLink().add(linkElement);
		}
	}
}
