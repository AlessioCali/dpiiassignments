package it.polito.dp2.NFV.sol1;

import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.sol1.jaxb.NodeType;

class NodeReaderImpl implements NodeReader {
	
	private NfvReaderImpl nfv;
	private NodeType nt;
	private String nffgName;
	
	protected NodeReaderImpl(NodeType nt, String nffgName, NfvReaderImpl nfv) {
		this.nt = nt;
		this.nffgName = nffgName;
		this.nfv = nfv;
	}

	@Override
	public String getName() { return nt.getName(); }

	@Override
	public VNFTypeReader getFuncType() { 
		return nfv.getVNFCatalog().stream()
								  .filter(vnf -> vnf.getName().equals(nt.getVnf()))
								  .findFirst()
								  .orElse(null);
	}

	@Override
	public HostReader getHost() { return nfv.getHost(nt.getHost()); }

	@Override
	public Set<LinkReader> getLinks() {
		return nt.getLink().stream()
						   .map(lt -> new LinkReaderImpl(lt, nfv, this))
						   .collect(Collectors.toSet());
	}

	@Override
	public NffgReader getNffg() {
		return nfv.getNffg(nffgName);
	}

	protected String getNffgName() {
		return nffgName;
	}
}
