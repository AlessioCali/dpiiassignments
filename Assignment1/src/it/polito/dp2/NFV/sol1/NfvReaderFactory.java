package it.polito.dp2.NFV.sol1;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NfvReaderException;
import it.polito.dp2.NFV.sol1.jaxb.INType;

public class NfvReaderFactory extends it.polito.dp2.NFV.NfvReaderFactory {
	
	private static final String NFV_SCHEMA_LOC = "xsd/nfvInfo.xsd";

	@Override
	public NfvReader newNfvReader() throws NfvReaderException {
		String filename = System.getProperty("it.polito.dp2.NFV.sol1.NfvInfo.file");
		BufferedInputStream in = null;
		BufferedInputStream schemaFile = null;
		Schema s = null;
		
		if (filename == null) {
			throw new NfvReaderException("Property it.polito.dp2.NFV.sol1.NfvInfo.file is not set.");
		}
		
		try {
			// Open input XML
			in = new BufferedInputStream(new FileInputStream(filename));
			
			// Load schema
			SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
			schemaFile = new BufferedInputStream(new FileInputStream(NFV_SCHEMA_LOC));
			s = sf.newSchema(new StreamSource(schemaFile));
			
			// Create context and unmarshal source file
			JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.NFV.sol1.jaxb");
			Unmarshaller u = jc.createUnmarshaller();
			u.setSchema(s);
			
			@SuppressWarnings("unchecked")
			JAXBElement<INType> root = (JAXBElement<INType>)u.unmarshal(new StreamSource(in));
			return new NfvReaderImpl(root.getValue());
		}
		catch (SAXException se) {
			throw new NfvReaderException(se, "Error in schema file");
		}
		catch (FileNotFoundException fnfe) {
			throw new NfvReaderException(fnfe, "Couldn't locate input file.");
		} 
		catch (JAXBException je) {
			throw new NfvReaderException(je, "Error during JAXB operations");
		}
		catch (ClassCastException ce) {
			throw new NfvReaderException(ce, "Unexpected XML root");
		}
		catch (Exception e) {
			throw new NfvReaderException(e, "Unexpected exception");
		} 
		finally {
			if (in != null) {
				try { in.close(); }
				catch (IOException ioe) { ioe.printStackTrace(); }
			}
			if (schemaFile != null) {
				try { schemaFile.close(); }
				catch (IOException ioe) { ioe.printStackTrace(); }
			}
		}
	}
}
