# Distributed Programming II Assignments - A.Y. 2017/2018

This is the result of many months of tiring work. You who are facing the same predicaments as I did, and working your way out of the infamous DPII Assignments, worry not, for we have all been there. You can do it, but it's going to be an impervious journey. Know that even though my path was different than yours, you can at least take inspiration and use my work in any way you deem useful. Good luck and may that be a trentaellode.

## What this is ACTUALLY about

This repository holds a number of mandatory exercises and assignments which ultimately led to the development of a java based web service during the Distributed Programming II course held at the Polythechnic University of Turin during academical year 2017/2018. A lot of effort (and swearing) has been put into it, I hope it will serve somehow future students.