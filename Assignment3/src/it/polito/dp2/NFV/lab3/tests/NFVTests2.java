package it.polito.dp2.NFV.lab3.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NfvReaderException;
import it.polito.dp2.NFV.NfvReaderFactory;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.lab3.AllocationException;
import it.polito.dp2.NFV.lab3.DeployedNffg;
import it.polito.dp2.NFV.lab3.NfvClient;
import it.polito.dp2.NFV.lab3.NfvClientFactory;
import it.polito.dp2.NFV.lab3.ServiceException;

public class NFVTests2 {

	private static NfvReaderFactory readerFactory;
	private static NfvClient client;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("it.polito.dp2.NFV.lab3.URL", "http://localhost:8080/NfvDeployer/rest");
		System.setProperty("it.polito.dp2.NFV.NfvReaderFactory", "it.polito.dp2.NFV.sol3.client2.NfvReaderFactory");
		System.setProperty("it.polito.dp2.NFV.NfvClientFactory", "it.polito.dp2.NFV.sol3.client1.NfvClientFactory");
		
		readerFactory = NfvReaderFactory.newInstance();
		client = NfvClientFactory.newInstance().newNfvClient();
		assertNotNull("NfvReader generated a null instance", readerFactory);
		assertNotNull("NfvClient generated a null instance", client);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
/*
	@Test
	public void TestConcurrency() throws Exception {
		final DeployedNffg nffg = client.getDeployedNffg("Nffg0");
		final NfvReader reader = readerFactory.newNfvReader();
		
		final VNFTypeReader vnf = reader.getVNFCatalog().iterator().next();
		final HostReader hr = reader.getHosts().iterator().next();
		
		Thread t1 = new Thread() {
			@Override
			public void run() {
				try {
					NodeReader n = nffg.addNode(vnf, hr.getName());
					System.out.println("Writer done. Node name : " + n.getName());
				} catch (AllocationException | ServiceException e) {
					e.printStackTrace();
				}
			}
		};
		
		Thread t2 = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(100);
					NfvReader reader2 = readerFactory.newNfvReader();
					System.out.println("Reader done");					
				}
				catch (Exception e) { e.printStackTrace(); }
			}
		};
		
		t1.start(); t2.start();
		t1.join(); t2.join();
	}

	@Test
	public void TestHostLoad() throws Exception {
		NfvReader reader = readerFactory.newNfvReader();
		DeployedNffg nffg = client.getDeployedNffg("Nffg0");
		
		Iterator<VNFTypeReader> it = reader.getVNFCatalog().iterator();
		VNFTypeReader deployedVnf = it.next();		
		while(it.hasNext()) {
			VNFTypeReader next = it.next();
			if (next.getRequiredStorage() < deployedVnf.getRequiredStorage()) {
				deployedVnf = next;
			}
		}
		
		try {
			for (int i = 0 ; i < 1000 ; i++) { nffg.addNode(deployedVnf, null); }
		}
		catch (AllocationException ae) { System.out.println("Saturated resources"); }
		catch (ServiceException se) { se.printStackTrace(); }
		
		reader = readerFactory.newNfvReader();
		for (HostReader hr : reader.getHosts()) {
			int memoryMB = 0;
			int storageMB = 0;
			
			Set<NodeReader> nodes = hr.getNodes();
			
			assertTrue("More VNFs than possible", nodes.size() <= hr.getMaxVNFs());
			
			for (NodeReader nr : hr.getNodes()) {
				VNFTypeReader vnf = nr.getFuncType();
				memoryMB += vnf.getRequiredMemory();
				storageMB += vnf.getRequiredStorage();
			}
			
			assertTrue("Memory overload", memoryMB <= hr.getAvailableMemory());
			assertTrue("Storage overload", storageMB <= hr.getAvailableStorage());
		}
	}
*/
	
	@Test(expected = NfvReaderException.class)
	public void TestUnreachable () throws Exception {
		NfvReader reader = readerFactory.newNfvReader();
	}
}
