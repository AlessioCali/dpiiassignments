package it.polito.dp2.NFV.sol3.service;

public class AllocationException extends Exception {

	private static final long serialVersionUID = -4726357453783509853L;

	public AllocationException() {
	}

	public AllocationException(String arg0) {
		super(arg0);
	}

	public AllocationException(Throwable arg0) {
		super(arg0);
	}

	public AllocationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public AllocationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
