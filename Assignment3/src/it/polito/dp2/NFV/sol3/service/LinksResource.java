package it.polito.dp2.NFV.sol3.service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import it.polito.dp2.NFV.sol3.service.jaxb.Link;
import it.polito.dp2.NFV.sol3.service.jaxb.Links;

@Path("nffgs/{nffgName}/nodes/{nodeName}/links")
public class LinksResource {

	@PathParam("nffgName")
	private String nffgName;
	
	@PathParam("nodeName")
	private String nodeName;
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Links getLinks() {
		try {
			return BackendResources.getSingleton().getLinks(nffgName, nodeName);
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException(); 
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response addNewLink(Link link, @QueryParam("overwrite") @DefaultValue("false") boolean overwrite) {
		if (link == null) { throw new BadRequestException(); }

		try {
			Link lt = BackendResources.getSingleton().addLink(nffgName, nodeName, link, overwrite);
			return Response.status(201)
						   .type(MediaType.APPLICATION_XML)
						   .entity(lt)
						   .build();
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (BadInputException bi) { throw new BadRequestException(); }
		catch (ResourceConflictException rc) { throw new WebApplicationException(409); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@DELETE
	@Path("{linkName}")
	public Response deleteLink(@PathParam("linkName") String linkName) {
		throw new WebApplicationException(501);
	}
}
