package it.polito.dp2.NFV.sol3.service;

public class BackendInitException extends Exception {

	private static final long serialVersionUID = 709173780634894464L;

	public BackendInitException() {

	}

	public BackendInitException(String message) {
		super(message);
	}

	public BackendInitException(Throwable cause) {
		super(cause);
	}

	public BackendInitException(String message, Throwable cause) {
		super(message, cause);
	}

	public BackendInitException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
