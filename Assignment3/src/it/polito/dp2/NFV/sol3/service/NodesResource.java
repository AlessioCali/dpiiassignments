package it.polito.dp2.NFV.sol3.service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import it.polito.dp2.NFV.sol3.service.jaxb.HostRefs;
import it.polito.dp2.NFV.sol3.service.jaxb.Node;
import it.polito.dp2.NFV.sol3.service.jaxb.NodeRef;
import it.polito.dp2.NFV.sol3.service.jaxb.Nodes;

@Path("nffgs/{nffgName}/nodes")
public class NodesResource {

	@PathParam("nffgName")
	private String nffgName;
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Nodes getNffgNodes() {
		try {			
			return BackendResources.getSingleton().getNodes(nffgName);
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response addNewNode(Node node) {
		if (node == null) { throw new BadRequestException(); }

		try {			
			NodeRef ref = BackendResources.getSingleton().addNode(nffgName, node);
			return Response.status(201)
						   .type(MediaType.APPLICATION_XML)
						   .contentLocation(UriBuilder.fromUri(ref.getHref()).build())
						   .entity(ref)
						   .build();
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (BadInputException bi) { throw new BadRequestException(); }
		catch (ResourceConflictException rc) { throw new WebApplicationException(409); }
		catch (AllocationException ae) { throw new WebApplicationException(507); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@GET
	@Path("{nodeName}")
	@Produces(MediaType.APPLICATION_XML)
	public Node getNode(@PathParam("nodeName") String nodeName) {
		try {
			Node nt = BackendResources.getSingleton().getNode(nffgName, nodeName);
			return nt;
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			throw new InternalServerErrorException();
		}
	}
	
	@DELETE
	@Path("{nodeName}")
	public Response removeNode(@PathParam("nodeName") String nodeName) {
		throw new WebApplicationException(501);
	}
	
	@GET
	@Path("{nodeName}/reachableHosts")
	@Produces(MediaType.APPLICATION_XML)
	public HostRefs getReachableHosts(@PathParam("nodeName") String nodeName) {
		try {
			return BackendResources.getSingleton().getReachableHosts(nffgName, nodeName);
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
}
