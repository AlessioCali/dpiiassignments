package it.polito.dp2.NFV.sol3.service;

public class ResourceConflictException extends Exception {

	private static final long serialVersionUID = 6455647782779162781L;

	public ResourceConflictException() { }

	public ResourceConflictException(String message) {
		super(message);
	}

	public ResourceConflictException(Throwable cause) {
		super(cause);
	}

	public ResourceConflictException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceConflictException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
