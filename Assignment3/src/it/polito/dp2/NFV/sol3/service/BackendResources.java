package it.polito.dp2.NFV.sol3.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.stream.Collectors;

import javax.ws.rs.core.UriBuilder;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import it.polito.dp2.NFV.ConnectionPerformanceReader;
import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NfvReaderFactory;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.sol3.service.jaxb.*;

/**
 * <p>This class is a singleton which initializes, keeps track of and updates
 * the internal structures of the Dp2 Nfv System. Resource classes (to which
 * clients make requests) access the internal data through its methods.</p>
 * 
 * <p>All methods accessing mutable data (that is, everything except the host list,
 * connections and the VNF catalog) are guarded through a fair {@link ReentrantReadWriteLock}.
 * This means that, in principle, multiple GET requests can be satisfied concurrently,
 * while each POST request is exclusive with respect to other GET/POSTs. The actual
 * scheduling policy is defined by the ReentrantLock implementation.</p>
 * 
 * <p>Furthermore, since referenced data is no longer locked after being returned
 * the calling class is usually given a copy of the original resource, if this is
 * mutable.</p>
 * 
 * <p>If an error occurs during the initialization of the singleton, the exception thrown
 * is stored and thrown back whenever trying to access the singleton itself.</p>
 * 
 * @author Alessio
 *
 */
public class BackendResources {

	private static BackendResources singleton = null;
	
	private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
	private final ReadLock readLock = readWriteLock.readLock();
	private final WriteLock writeLock = readWriteLock.writeLock();
	
	private URI baseUri = UriBuilder.fromUri("/").build();
	private Neo4JSimpleXMLInterface neo4jInterface;
	
	private VNFCatalog catalog = new VNFCatalog();
	
	private HostRefs hostRefs = new HostRefs();
	private ConcurrentHashMap<String, HostManager> hosts = new ConcurrentHashMap<>();
	
	private NFFGRefs nffgRefs = new NFFGRefs();
	private ConcurrentHashMap<String, NFFGManager> nffgs = new ConcurrentHashMap<>();
	private HashSet<String> nodeNameSet = new HashSet<>();
	
	private BackendResources() throws BackendInitException {
		try {
			String neo4jUri = System.getProperty("it.polito.dp2.NFV.lab3.Neo4JSimpleXMLURL", "http://localhost:8080/Neo4JSimpleXML/rest");
			neo4jInterface = new Neo4JSimpleXMLInterface(UriBuilder.fromUri(neo4jUri).build());
			
			NfvReaderFactory factory = NfvReaderFactory.newInstance();
			NfvReader reader = factory.newNfvReader();
			
			loadCatalog(reader.getVNFCatalog());
			loadHosts(reader);
			loadNffg(reader.getNffg("Nffg0"));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new BackendInitException("Internal initialization error", e);
		}
	}
	
	private void loadCatalog(Set<VNFTypeReader> vnfSet) {
		for (VNFTypeReader vnft : vnfSet) {
			VNFType vnf = new VNFType();
			vnf.setName(vnft.getName());
			vnf.setMemoryMB(vnft.getRequiredMemory());
			vnf.setStorageMB(vnft.getRequiredStorage());
			vnf.setType(VNFCode.fromValue(vnft.getFunctionalType().value()));
			catalog.getVNF().add(vnf);
		}
	}

	private void loadHosts(NfvReader reader) throws Neo4JException {
		Set<HostReader> hostSet = reader.getHosts();
		
		for (HostReader hr : hostSet) {
			Host ht = new Host();
			Connections cs = new Connections();
			Allocations as = new Allocations();
			
			// Load host data
			ht.setAllocations(as);
			ht.setConnections(cs);
			ht.setMaxVNF(hr.getMaxVNFs());
			ht.setMemoryMB(hr.getAvailableMemory());
			ht.setName(hr.getName());
			ht.setStorageMB(hr.getAvailableStorage());
			
			// Load connections
			for (HostReader dst : hostSet) {
				ConnectionPerformanceReader cp = reader.getConnectionPerformance(hr, dst);
				if (cp != null) {
					CPType cpt = new CPType();
					URI uri = UriBuilder.fromUri(baseUri).path("hosts").path(dst.getName()).build();
					
					cpt.setLatency(cp.getLatency());
					cpt.setThroughput(cp.getThroughput());
					cpt.setTo(dst.getName());
					cpt.setHref(uri.toASCIIString());
					cs.getConnection().add(cpt);
				}
			}
			
			// Skip allocations for now, they will be created while loading Nffg(s)
			
			hosts.put(hr.getName(), new HostManager(ht));
			
			// Generate reference
			URI hostUri = UriBuilder.fromUri(baseUri).path("hosts").path(hr.getName()).build();
			ObjectReference ref = new ObjectReference();
			ref.setName(hr.getName());
			ref.setHref(hostUri.toASCIIString());
			hostRefs.getHostRef().add(ref);
			
			// Finally, generate Host Node on Neo4J
			neo4jInterface.postHost(ht);
		}
	}

	private void loadNffg(NffgReader nffgr) throws DatatypeConfigurationException, Neo4JException {
		DatatypeFactory calendarFactory = DatatypeFactory.newInstance();
		
		// Load NFFG data
		NFFG nffgt = new NFFG();
		GregorianCalendar deployTime = new GregorianCalendar();
		deployTime.setTime(nffgr.getDeployTime().getTime());
		nffgt.setName(nffgr.getName());
		nffgt.setDeployTime(calendarFactory.newXMLGregorianCalendar(deployTime));
	
		// Load nodes data
		for (NodeReader nr : nffgr.getNodes()) {
			Node nt = new Node();
			URI hostUri = UriBuilder.fromUri(baseUri)
									.path("hosts")
									.path(nr.getHost().getName())
									.build();
			URI srcUri = UriBuilder.fromUri(baseUri)
								   .path("nffgs")
								   .path(nffgr.getName())
								   .path("nodes")
								   .path(nr.getName())
								   .build();
			
			nt.setHost(nr.getHost().getName());
			nt.setHostRef(hostUri.toASCIIString());
			nt.setName(nr.getName());
			nt.setVnf(nr.getFuncType().getName());
			
			// Load links data
			for (LinkReader lr : nr.getLinks()) {
				Link lt = new Link();
				URI uri = UriBuilder.fromUri(baseUri)
									.path("nffgs")
									.path(nffgr.getName())
									.path("nodes")
									.path(lr.getDestinationNode().getName())
									.build();
				
				lt.setDest(lr.getDestinationNode().getName());
				lt.setLatency(lr.getLatency());
				lt.setName(lr.getName());
				lt.setThroughput(lr.getThroughput());
				lt.setDestRef(uri.toASCIIString());
				lt.setSrcRef(srcUri.toASCIIString());
				
				nt.getLink().add(lt);
			}
			
			// Allocate on Host
			HostManager hm = hosts.get(nt.getHost());
			
			if (hm != null) { // Just in case
				NAType nat = new NAType();
				URI uri = UriBuilder.fromUri(baseUri)
									.path("nffgs")
									.path(nffgr.getName())
									.path("nodes")
								 	.path(nr.getName())
								 	.build();
				
				nat.setNode(nr.getName());
				nat.setNffg(nr.getNffg().getName());
				nat.setHref(uri.toASCIIString());
				
				VNFType vnf = catalog.getVNF().stream()
											  .filter(v -> v.getName().equals(nr.getFuncType().getName()))
											  .findFirst()
											  .get();
				
				// No check is done, source data is assumed clean
				hm.queue(nat, vnf);
			}
			
			nffgt.getNode().add(nt);
			nodeNameSet.add(nt.getName());
		}
		
		// Commit allocations
		for (HostManager hm : hosts.values()) { hm.allocate(); }
		
		nffgs.put(nffgr.getName(), new NFFGManager(nffgt));
		
		URI nffgUri = UriBuilder.fromUri(baseUri).path("nffgs").path(nffgr.getName()).build();
		NFFGRef ref = new NFFGRef();
		ref.setName(nffgr.getName());
		ref.setHref(nffgUri.toASCIIString());
		nffgRefs.getNFFGRef().add(ref);
		
		// Now load NFFG structure onto Neo4j
		neo4jInterface.postNFFG(nffgt);
	}

	/**
	 * Returns this class' singleton. If an error occurred during initialization, it 
	 * is returned as a {@link BackendInitException}.
	 * 
	 * In case of failure, subsequent calls will try to initialize again the
	 * singleton.
	 * 
	 * @return The singleton
	 * @throws BackendInitException If an error occurred during initialization
	 */
	public synchronized static BackendResources getSingleton() throws BackendInitException {
		if (singleton == null) { singleton = new BackendResources(); }
		return singleton;
	}
	
	/**
	 * Returns the VNFCatalog. Does not require synchronization since it's immutable.
	 * 
	 * @return The VNF Catalog.
	 */
	public VNFCatalog getVNFCatalog() {
		return catalog;
	}
	
	/**
	 * Returns the list of references to hosts. Does not require synchronization
	 * since it's immutable.
	 * 
	 * @return The list of references to hosts.
	 */
	public HostRefs getHostRefs() {
		return hostRefs;
	}
	
	/**
	 * <p>Returns a specific host's data. Since the allocation list is mutable
	 * a copy of the original data is returned.</p>
	 * 
	 * <p>Note: while the allocation <em>list</em> is mutable, the allocations
	 * themselves are not. As such the current list's element are added in bulk
	 * to the copy's.</p> 
	 * 
	 * @param hostName The name of the host to return.
	 * @return A copy of the given host data.
	 * @throws UnknownEntityException if no host with that name exists.
	 */
	public Host getHost(String hostName) throws UnknownEntityException {
		HostManager hm = hosts.get(hostName);
		
		if (hm != null) {
			// Data replication: allocations
			Host ht = hm.getHost();
			Host copy = new Host();
			Allocations as = ht.getAllocations();
			Allocations asCopy = new Allocations();
			
			readLock.lock();
			try { asCopy.getNodeAllocation().addAll(as.getNodeAllocation()); }
			finally { readLock.unlock(); }
			
			copy.setAllocations(asCopy);
			copy.setConnections(ht.getConnections());
			copy.setMaxVNF(ht.getMaxVNF());
			copy.setMemoryMB(ht.getMemoryMB());
			copy.setName(ht.getName());
			copy.setStorageMB(ht.getStorageMB());
			
			return copy;
		}
		else {
			throw new UnknownEntityException("No host with name " + hostName);
		}
	}
	
	/**
	 * Acts similarly to {@link #getHost(String)}, but only returns allocations.
	 * 
	 * @param hostName The host whose Allocations are requested.
	 * @return A copy of the host's allocations.
	 * @throws UnknownEntityException if no host with that name exists.
	 */
	public Allocations getAllocations(String hostName) throws UnknownEntityException {
		HostManager hm = hosts.get(hostName);
		
		if (hm != null) {
			Allocations as = hm.getHost().getAllocations();
			Allocations asCopy = new Allocations();
			
			readLock.lock();
			try { asCopy.getNodeAllocation().addAll(as.getNodeAllocation()); }
			finally { readLock.unlock(); }
			
			return asCopy;
		}
		else {
			throw new UnknownEntityException("No host with name " + hostName);
		}
	}
	
	/**
	 * <p>Acts similarly to {@link #getHost(String)}, but only returns connections.</p>
	 * <p>Since connections are assumed to be immutable, this method is not
	 * synchronized, and the real connections are returned.</p>
	 * 
	 * @param hostName The host whose Connections are requested.
	 * @param dest The destination host name.
	 * @return A list of connections encapsulating the connection going from hostName to dest,
	 * or all connections starting from hostName if no destination is specified.
	 * @throws UnknownEntityException if source or destination host names are unknown.
	 */
	public Connections getConnections(String hostName, String dest) throws UnknownEntityException {
		HostManager hm = hosts.get(hostName);
		
		if (hm != null) {
			Host ht = hm.getHost();
			
			// No destination specified, return all connections
			if (dest == null) { return ht.getConnections(); }
			else {
				// Find desired connection
				CPType cp = ht.getConnections().getConnection().stream()
															   .filter(conn -> conn.getTo().equals(dest))
															   .findFirst()
															   .orElse(null);
				
				// No match, NotFound
				if (cp == null) { throw new UnknownEntityException("No host with name " + dest + " is linked with host " + hostName); }
				else {
					// Return ConnectionsType encapsulating the found CPType
					Connections ct = new Connections();
					ct.getConnection().add(cp);
					return ct;
				}
			}
		}
		else { throw new UnknownEntityException("No host with name " + hostName); }
	}
	
	/**
	 * @param since The minimum deployment date for the Nffg to return
	 * @return a snapshot of the reference list to all Nffgs deployed
	 * after the given date, or to all Nffgs if no date is given.
	 */
	public NFFGRefs getNffgRefs(Calendar since) {
		NFFGRefs refs = new NFFGRefs();
		
		readLock.lock();
		try {
			// No starting date, return all NFFG
			if (since == null) {
				refs.getNFFGRef().addAll(nffgRefs.getNFFGRef());
			}
			else {	// Filter only references to NFFGs deployed AFTER the given date.
				Set<NFFGRef> refSet = nffgRefs.getNFFGRef()
													  .stream()
													  .filter(r -> nffgs.get(r.getName())
															  			.getNffg()
															  			.getDeployTime()
															  			.toGregorianCalendar()
															  			.after(since)
													  )
													  .collect(Collectors.toSet());
				refs.getNFFGRef().addAll(refSet);
			}
		}
		finally { readLock.unlock(); }
		
		return refs;
	}
	
	/**
	 * @param nffgName The Nffg to get
	 * @return A snapshot of links to the nodes currently deployed
	 * on this Nffg, plus the Nffg informations.
	 * @throws UnknownEntityException if no Nffg with that name is found.
	 */
	public NFFG getNffg(String nffgName) throws UnknownEntityException {
		NFFG copy = new NFFG();
		
		readLock.lock();
		try {
			NFFGManager nffgm = nffgs.get(nffgName);
			
			// NFFG not found
			if (nffgm == null) { throw new UnknownEntityException("No Nffg with name " + nffgName); }
			else {
				NFFG nffgt = nffgm.getNffg();
				copy.setDeployTime(nffgt.getDeployTime());
				copy.setName(nffgt.getName());
				
				for (Node nt : nffgt.getNode()) {
					copy.getNode().add(cloneNode(nt));
				}
				
				return copy;
			}
		}
		finally { readLock.unlock(); }
	}
	
	/**
	 * 
	 * @param nffg the Nffg to deploy
	 * @return A reference to the deployed Nffg.
	 * @throws DatatypeConfigurationException If it is not possible to generate a factory to convert the current date. See {@link DatatypeFactory#newInstance}.
	 * @throws BadInputException If the Nffg data is invalid, e.g. references an invalid VNF or links to nodes not in this Nffg.
	 * @throws ResourceConflictException If any name conflict arises (existing Nffg or Node names).
	 * @throws AllocationException If it's impossible to allocate all nodes of the Nffg.
	 * @throws Neo4JException If an error occurs while updating Neo4J with the deployed Nffg.
	 */
	public NFFGRef deployNffg(NFFG nffg) throws DatatypeConfigurationException, BadInputException, ResourceConflictException, AllocationException, Neo4JException {	
		writeLock.lock();
		try {
			// There is already an Nffg with that name
			if (nffgs.containsKey(nffg.getName())) { throw new ResourceConflictException("An Nffg named " + nffg.getName() + " already exists."); }
			else {
				// Keeps track of all hosts which will be used for deployment
				HashSet<HostManager> targetHosts = new HashSet<>();
				
				try {
					HashSet<Node> nodesToAdd = new HashSet<>();
					HashMap<String, HashSet<Link>> links = new HashMap<>();
					
					for (Node nt : nffg.getNode()) {
						// Save links to add them later
						links.put(nt.getName(), new HashSet<>(nt.getLink()));
						
						HostManager targetHost = tryAllocate(nt, nffg.getName());
						if (targetHost == null) { throw new AllocationException("Could not allocate node " + nt.getName()); }
						
						targetHosts.add(targetHost);
						nodesToAdd.add(nt);
					}
					
					// Deploy to Neo4J
					neo4jInterface.postNFFG(nffg);
					
					// Set deploy time
					nffg.setDeployTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
					
					// Create NFFG reference
					NFFGRef ref = new NFFGRef();
					URI uri = UriBuilder.fromUri(baseUri).path("nffgs").path(nffg.getName()).build();
					ref.setName(nffg.getName());
					ref.setHref(uri.toASCIIString());
					
					// Add new Manager for Nffg
					NFFGManager nffgm = new NFFGManager(nffg);
					nffgs.put(nffg.getName(), nffgm);
					
					// Add all links
					for (Node node : nodesToAdd) { 
						for (Link lt : links.get(node.getName())) { 
							addLinkInternal(nffgm, node, lt, false); 
						}
					}
					
					// Commit changes by adding new references and node names and confirm allocation
					nffgRefs.getNFFGRef().add(ref);
					nodeNameSet.addAll(
							nodesToAdd.stream()
								      .map(nt -> nt.getName())
									  .collect(Collectors.toSet())
					);
					for (HostManager hm : targetHosts) { hm.allocate(); }
					
					return ref;
				}
				catch (Exception e) {
					// Rollback status in case of exception
					nffgs.remove(nffg.getName());
					for (HostManager hm : targetHosts) { hm.clearQueue(); }
					throw e;
				}
			}
		}
		finally { writeLock.unlock(); }
	}

	/**
	 * Tries to find a candidate host for allocation of a given node. The research algorithm
	 * is a simple greedy look-up. Links inside the Node are eliminated as a consequence 
	 * of this procedure.
	 * 
	 * @param nt The node to allocate
	 * @param nffgName The nffg this node belongs to (to build hyperlinks).
	 * @return The HostManager of the target host, if found, else null.
	 * @throws ResourceConflictException If a node with that name already exists.
	 * @throws BadInputException If the node points to an invalid VNF.
	 */
	private HostManager tryAllocate(Node nt, String nffgName) throws ResourceConflictException, BadInputException {
		// Check if existing node name
		if (nodeNameSet.contains(nt.getName())) { throw new ResourceConflictException("A node with name " + nt.getName() + " already exists."); }
		
		// Check requested VNF actually exists
		VNFType nodeVnf = catalog.getVNF().stream()
										  .filter(vnf -> vnf.getName().equals(nt.getVnf()))
										  .findFirst()
										  .orElse(null);
		
		if (nodeVnf == null) { throw new BadInputException("No Vnf with name " + nt.getVnf() + " exists."); }

		// Delete any suggested link, if present
		nt.getLink().clear();		
		
		// Scan the host list for allocation
		ArrayList<HostManager> toCheck = new ArrayList<>(hosts.values());
		
		// If a host was suggested, move it to the first position
		if (nt.getHost() != null) {
			HostManager hm = hosts.get(nt.getHost());
			if (hm != null) {
				int idx = toCheck.indexOf(hm);
				toCheck.set(idx, toCheck.get(0));
				toCheck.set(0, hm);
			}
		}
		
		for (HostManager hm : toCheck) {
			if (hm.canAllocate(nodeVnf)) {
				// Allocation is possible, queue new allocation
				NAType na = new NAType();
				URI href = UriBuilder.fromUri(baseUri).path("nffgs").path(nffgName).path("nodes").path(nt.getName()).build();
				na.setHref(href.toASCIIString());
				na.setNffg(nffgName);
				na.setNode(nt.getName());				
				
				URI hostRef = UriBuilder.fromUri(baseUri).path("hosts").path(hm.getHost().getName()).build();
				hm.queue(na, nodeVnf);
				nt.setHost(hm.getHost().getName());
				nt.setHostRef(hostRef.toASCIIString());
				return hm;
			}
		}
		
		return null;
	}

	/**
	 * @param nffgName The Nffg to get
	 * @return A snapshot of links to the nodes currently deployed
	 * on this Nffg.
	 * @throws UnknownEntityException if no Nffg with that name is found.
	 */
	public Nodes getNodes(String nffgName) throws UnknownEntityException {
		readLock.lock();
		try {
			NFFGManager nffgm = nffgs.get(nffgName);
			if (nffgm == null) { throw new UnknownEntityException("No Nffg with name " + nffgName + " exists."); }
			
			NFFG nffg = nffgm.getNffg();
			Nodes nodes = new Nodes();
			for (Node nt : nffg.getNode()) {
				nodes.getNode().add(cloneNode(nt));
			}
			
			return nodes;
		}
		finally { readLock.unlock(); }		
	}
	
	private Node cloneNode(Node nt) {
		Node ntCopy = new Node();
		ntCopy.setHost(nt.getHost());
		ntCopy.setName(nt.getName());
		ntCopy.setVnf(nt.getVnf());
		ntCopy.getLink().addAll(cloneLinks(nt.getLink()));
		ntCopy.setHostRef(nt.getHostRef());
		return ntCopy;
	}
	
	private Collection<Link> cloneLinks(Collection<Link> src) {
		return src.stream().map(lt -> {
			Link copy = new Link();
			copy.setDest(lt.getDest());
			copy.setDestRef(lt.getDestRef());
			copy.setLatency(lt.getLatency());
			copy.setName(lt.getName());
			copy.setSrcRef(lt.getSrcRef());
			copy.setThroughput(lt.getThroughput());
			return copy;
		})
		.collect(Collectors.toSet());
	}
	
	/**
	 * 
	 * @param nffgName The name of the Nffg to which add the new Node.
	 * @param node The Node to add.
	 * @return A reference to the added Node.
	 * @throws UnknownEntityException If the Nffg name does not belong to any known Nffg.
	 * @throws ResourceConflictException If a Node with the same name already exists.
	 * @throws AllocationException If it's not possible to allocate the node.
	 * @throws BadInputException If the node data is invalid (e.g. invalid VNF).
	 * @throws Neo4JException If an error occurs while updating the Neo4J service with the new Node.
	 */
	public NodeRef addNode(String nffgName, Node node) throws UnknownEntityException, ResourceConflictException, AllocationException, BadInputException, Neo4JException {
		writeLock.lock();
		try {
			NFFGManager nffgm = nffgs.get(nffgName);
			if (nffgm == null) { throw new UnknownEntityException("No Nffg with name " + nffgName + " exists."); }
			
			HostManager target = tryAllocate(node, nffgName);
			try {
				if (target == null) { throw new AllocationException("Could not allocate node " + node.getName()); }
				nffgm.getNffg().getNode().add(node);
				nodeNameSet.add(node.getName());
				
				// Deploy to Neo4J
				neo4jInterface.postNffgNode(node, nffgName);
				
				target.allocate();
				
				NodeRef ref = new NodeRef();
				URI uri = UriBuilder.fromUri(baseUri).path("nffgs").path(nffgName).path("nodes").path(node.getName()).build();
				ref.setName(node.getName());
				ref.setHref(uri.toASCIIString());
				return ref;
			}
			catch (Exception e) {
				if (target != null) { target.clearQueue(); }
				throw e;
			}
		}
		finally { writeLock.unlock(); }
	}
	
	/**
	 * @param nffgName The nffg of the node to get
	 * @param nodeName The node to get
	 * @return A copy of the node to get.
	 * @throws UnknownEntityException If the Nffg/Node pair is unknown
	 */
	public Node getNode(String nffgName, String nodeName) throws UnknownEntityException {
		readLock.lock();
		try {
			NFFGManager nffgm = nffgs.get(nffgName);
			if (nffgm == null) { throw new UnknownEntityException("No Nffg with name " + nffgName + " exists."); }
			
			Node nt = findNode(nffgm.getNffg(), nodeName);
			if (nt == null) { throw new UnknownEntityException("No Node with name " + nodeName + " exists within Nffg " + nffgName); }
			
			return cloneNode(nt);
		}
		finally { readLock.unlock(); }
	}
	
	private Node findNode(NFFG nffg, String nodeName) {
		return nffg.getNode().stream()
							 .filter(n -> n.getName().equals(nodeName))
							 .findFirst()
							 .orElse(null);
	}
	
	public Links getLinks(String nffgName, String nodeName) throws UnknownEntityException {
		readLock.lock();
		try {
			NFFGManager nffgm = nffgs.get(nffgName);
			if (nffgm == null) { throw new UnknownEntityException("No Nffg with name " + nffgName + " exists."); }
			
			Node nt = findNode(nffgm.getNffg(), nodeName);
			if (nt == null) { throw new UnknownEntityException("No Node with name " + nodeName + " exists within Nffg " + nffgName); }
			
			Links links = new Links();
			links.getLink().addAll(nt.getLink());
			return links;			
		}
		finally { readLock.unlock(); }
	}
	
	private Link addLinkInternal(NFFGManager nffgm, Node src, Link link, boolean overwrite) throws ResourceConflictException, BadInputException, Neo4JException {
		Node dest = findNode(nffgm.getNffg(), link.getDest());
		if (dest == null) { throw new BadInputException("Destination node " + link.getDest() + " was not found in Nffg " + nffgm.getNffg().getName()); }
		
		String nffgName = nffgm.getNffg().getName();
		String srcName = src.getName();
		
		Link sourceLink;	// The link whose copy will be returned
		
		Link matching = src.getLink().stream()
										.filter(l -> l.getDest().equals(link.getDest()))
										.findFirst()
										.orElse(null);

		// Is there a link with that name?
		if (nffgm.containsLink(link.getName())) {			
			// YES: Is it the matching link? (same destination)
			if (matching != null && matching.getName().equals(link.getName())) {				
				// YES: Should I overwrite?
				if (overwrite) {
					// YES: Overwrite
					sourceLink = matching;
					matching.setLatency(link.getLatency());
					matching.setThroughput(link.getThroughput());
				}
				// NO: Link already present (same name, same dest, no overwrite)
				else { throw new ResourceConflictException(String.format("A link with name %s from node %s to %s already exists.", link.getName(), src.getName(), link.getDest())); }
			}
			// NO: Link already present (same name, other source OR other dest)
			else { throw new ResourceConflictException("A link named " + link.getName() + " already exists within Nffg " + nffgName); }
		}
		else {
			// NO: Is there already a link with the same destination?
			if (matching != null) {		
				// YES: Should I overwrite?
				if (overwrite) {
					// YES: Overwrite
					sourceLink = matching;
					matching.setName(link.getName());
					matching.setLatency(link.getLatency());
					matching.setThroughput(link.getThroughput());
				}
				// NO: Link already present (different name, same dest, same source)
				else { throw new ResourceConflictException(String.format("A link from node %s to %s already exists with a different name.", src.getName(), link.getDest())); }
			}
			else {
				// NO: Add a new link
				// Deploy on Neo4j
				neo4jInterface.forwardsTo(srcName, link.getDest(), nffgName);
				
				URI destUri = UriBuilder.fromUri(baseUri)
										.path("nffgs")
										.path(nffgName)
										.path("nodes")
										.path(dest.getName())
										.build();
				
				URI srcUri = UriBuilder.fromUri(baseUri)
									   .path("nffgs")
									   .path(nffgName)
									   .path("nodes")
									   .path(srcName)
									   .build();
				
				link.setSrcRef(srcUri.toASCIIString());
				link.setDestRef(destUri.toASCIIString());
				
				// Initialize default values if needed
				link.getLatency();
				link.getThroughput();
				
				nffgm.addLink(link);
				src.getLink().add(link);
				
				sourceLink = link;
			}
		}
		
		// Make a copy of the link and return it
		Link copy = new Link();
		copy.setSrcRef(sourceLink.getSrcRef());
		copy.setDest(sourceLink.getDest());
		copy.setDestRef(sourceLink.getDestRef());
		copy.setLatency(sourceLink.getLatency());
		copy.setName(sourceLink.getName());
		copy.setThroughput(sourceLink.getThroughput());
		return copy;
	}

	/**
	 * 
	 * @param nffgName The Nffg of the requested Node
	 * @param nodeName The Node to which add the new Link
	 * @param link The Link to add.
	 * @param overwrite Whether an existing Link should be overwritten
	 * @return The representation of the added Link
	 * @throws UnknownEntityException If the Nffg/Node pair is not known.
	 * @throws ResourceConflictException If a conflict arises which cannot be solved, or could be solved by overwriting an existing Link and overwrite is set to 'false'.
	 * @throws BadInputException If the destination Node does not exist.
	 * @throws Neo4JException If any problem arises while trying to update Neo4J.
	 */
	public Link addLink(String nffgName, String nodeName, Link link, boolean overwrite) throws UnknownEntityException, ResourceConflictException, BadInputException, Neo4JException {
		writeLock.lock();
		try {
			NFFGManager nffgm = nffgs.get(nffgName);
			if (nffgm == null) { throw new UnknownEntityException("No Nffg with name " + nffgName + " exists."); }
			
			Node nt = findNode(nffgm.getNffg(), nodeName);
			if (nt == null) { throw new UnknownEntityException("No Node named " + nodeName + " exists within Nffg " + nffgName); }
			
			return addLinkInternal(nffgm, nt, link, overwrite);
		}
		finally { writeLock.unlock(); }
	}
	
	/**
	 * Return the list of reachable hosts from the given Nffg Node
	 * 
	 * @param nffgName The Nffg of the desired Node
	 * @param nodeName The source Node
	 * @return The list of references to reachable hosts.
	 * @throws Neo4JException If an error occurs while quering Neo4J
	 * @throws UnknownEntityException If the Nffg/Node pair is unknown
	 */
	public HostRefs getReachableHosts(String nffgName, String nodeName) throws Neo4JException, UnknownEntityException {
		readLock.lock();
		try {
			// Check if Nffg/Node pair exists
			NFFGManager nffg = nffgs.get(nffgName);
			if (nffg == null) { throw new UnknownEntityException("No Nffg with name " + nffgName + " exists."); }
			else if (findNode(nffg.getNffg(), nodeName) == null) { 
				throw new UnknownEntityException("No Node named " + nodeName + " exists within Nffg " + nffgName); 
			}
			
			Set<String> hostNames = neo4jInterface.getReachableHosts(nffgName, nodeName);
			HostRefs refs = new HostRefs();
			Set<ObjectReference> filtered = hostRefs.getHostRef().stream()
																 .filter(hr -> hostNames.contains(hr.getName()))
																 .collect(Collectors.toSet());
			refs.getHostRef().addAll(filtered);
			return refs;
		}
		finally { readLock.unlock(); }
	}
}
