package it.polito.dp2.NFV.sol3.service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFV.sol3.service.jaxb.Link;
import it.polito.dp2.NFV.sol3.service.jaxb.NFFG;

public class NFFGManager {

	private final NFFG nffg;
	private HashSet<String> linkNames = new HashSet<>();
	
	public NFFGManager(NFFG nffg) { 
		this.nffg = nffg;
		
		Set<String> linkNames = nffg.getNode().stream()
											  .map(n -> n.getLink())
											  .flatMap(ls -> ls.stream())
											  .map(l -> l.getName())
											  .collect(Collectors.toSet());
		this.linkNames.addAll(linkNames);
	}
	
	public NFFG getNffg() {
		return nffg;
	}
	
	public boolean containsLink(String linkName) {
		return linkNames.contains(linkName);
	}
	
	public void addLink(Link link) {
		linkNames.add(link.getName());
	}
}
