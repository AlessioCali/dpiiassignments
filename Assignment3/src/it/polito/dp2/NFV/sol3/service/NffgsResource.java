package it.polito.dp2.NFV.sol3.service;

import java.util.Calendar;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.DatatypeConverter;

import it.polito.dp2.NFV.sol3.service.jaxb.NFFG;
import it.polito.dp2.NFV.sol3.service.jaxb.NFFGRef;
import it.polito.dp2.NFV.sol3.service.jaxb.NFFGRefs;

@Path("nffgs")
public class NffgsResource {
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public NFFGRefs getNffgsByDate(@QueryParam("since") String since) {
		try {
			Calendar calendar = null;
			if (since != null) {
				try {
					calendar = DatatypeConverter.parseDateTime(since);
				}
				catch (IllegalArgumentException ie) {
					throw new BadRequestException();
				}
			}
			return BackendResources.getSingleton().getNffgRefs(calendar);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML      )
	public Response deployNffg(NFFG nffg) {
		if (nffg == null) { throw new BadRequestException(); }

		try {
			NFFGRef ref = BackendResources.getSingleton().deployNffg(nffg);
			return Response.status(201)
						   .type(MediaType.APPLICATION_XML)
						   .contentLocation(UriBuilder.fromUri(ref.getHref()).build())
						   .entity(ref)
						   .build();
		}
		catch (BadInputException bi) { throw new BadRequestException(); }
		catch (ResourceConflictException rc) { throw new WebApplicationException(409); }
		catch (AllocationException ae) { throw new WebApplicationException(507); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@GET
	@Path("{nffgName}")
	@Produces(MediaType.APPLICATION_XML)
	public NFFG getNffgByName(@PathParam("nffgName") String nffgName) {
		try {
			NFFG nffgt = BackendResources.getSingleton().getNffg(nffgName);
			return nffgt;
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@DELETE
	@Path("{nffgName}")
	public void undeployNffg(@PathParam("nffgName") String nffgName) {
		throw new WebApplicationException(501);
	}
}
