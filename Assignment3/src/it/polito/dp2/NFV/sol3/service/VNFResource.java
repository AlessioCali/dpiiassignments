package it.polito.dp2.NFV.sol3.service;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import it.polito.dp2.NFV.sol3.service.jaxb.VNFCatalog;

@Path("vnfs")
public class VNFResource {

	public VNFResource() {}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public VNFCatalog getVNFCatalog() {
		try {
			return BackendResources.getSingleton().getVNFCatalog();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
}
