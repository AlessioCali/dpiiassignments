package it.polito.dp2.NFV.sol3.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

@Provider
@Consumes(MediaType.APPLICATION_XML)
public class NfvDeployerValidationProvider<T> implements MessageBodyReader<T> {

	private static final String NFV_DEPLOYER_JAXB_PACKAGE = "it.polito.dp2.NFV.sol3.service.jaxb";
	private static final String NFV_SCHEMA_LOCATION = "NfvDeployer.xsd";
	
	private Logger logger;
	
	private JAXBContext context;
	private Schema schema;
	
	public NfvDeployerValidationProvider() {
		logger = Logger.getLogger(getClass().getName());
		
		try {
			context = JAXBContext.newInstance(NFV_DEPLOYER_JAXB_PACKAGE);
			
			InputStream nfvSchemaStream = getClass().getClassLoader().getResourceAsStream(NFV_SCHEMA_LOCATION);
			if (nfvSchemaStream == null) {
				logger.log(Level.SEVERE, "Schema file not found");
				throw new IOException();
			}
			
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			schema = sf.newSchema(new StreamSource(nfvSchemaStream));
		}
		catch (IOException | JAXBException | SAXException e) {
			logger.log(Level.SEVERE, "Validation provider initialization failed, service won't work correctly");
		}
	}
	
	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return type.getPackage().getName().equals(NFV_DEPLOYER_JAXB_PACKAGE);
	}

	@Override
	public T readFrom(
				Class<T> type, Type genericType, Annotation[] annotations, MediaType mediaType,
				MultivaluedMap<String, String> httpHeaders, InputStream entityStream
			)
			throws IOException, WebApplicationException {

		try {
			Unmarshaller unmarshaller = context.createUnmarshaller();
			unmarshaller.setSchema(schema);
			
			@SuppressWarnings("unchecked")
			T value = (T) unmarshaller.unmarshal(entityStream);
			return value;
		}
		catch (JAXBException | ClassCastException je) {
			throw new BadRequestException("Validation of entity body failed.");
		}
	}

}
