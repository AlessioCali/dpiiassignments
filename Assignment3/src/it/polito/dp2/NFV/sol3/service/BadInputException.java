package it.polito.dp2.NFV.sol3.service;

public class BadInputException extends Exception {

	private static final long serialVersionUID = 2616748414211001654L;

	public BadInputException() { }

	public BadInputException(String arg0) {
		super(arg0);
	}

	public BadInputException(Throwable arg0) {
		super(arg0);
	}

	public BadInputException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BadInputException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
