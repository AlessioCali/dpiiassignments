package it.polito.dp2.NFV.sol3.service;

public class Neo4JException extends Exception {

	private static final long serialVersionUID = -2136296166102128667L;

	public Neo4JException() {
	}

	public Neo4JException(String message) {
		super(message);
	}

	public Neo4JException(Throwable cause) {
		super(cause);
	}

	public Neo4JException(String message, Throwable cause) {
		super(message, cause);
	}

	public Neo4JException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
