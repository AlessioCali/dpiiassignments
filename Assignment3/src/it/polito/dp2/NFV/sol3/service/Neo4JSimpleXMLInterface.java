package it.polito.dp2.NFV.sol3.service;

import java.io.StringReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import it.polito.dp2.NFV.lab3.ServiceException;
import it.polito.dp2.NFV.sol3.service.jaxb.Host;
import it.polito.dp2.NFV.sol3.service.jaxb.Link;
import it.polito.dp2.NFV.sol3.service.jaxb.NFFG;
import it.polito.dp2.NFV.sol3.service.jaxb.Node;
import it.polito.dp2.NFV.sol3.service.neo4j.Labels;
import it.polito.dp2.NFV.sol3.service.neo4j.Nodes;
import it.polito.dp2.NFV.sol3.service.neo4j.Properties;
import it.polito.dp2.NFV.sol3.service.neo4j.Property;
import it.polito.dp2.NFV.sol3.service.neo4j.Relationship;

public class Neo4JSimpleXMLInterface {
	
	private static final String N4J_SCHEMA_PATH = "application.wadl/xsd0.xsd";
	
	private Client client;
	private WebTarget dataTarget;
	private JAXBContext context;
	private Validator validator;
	
	// A map that holds, for each NFFG name, the mapping between the NFFG nodes and N4J Node Ids
	private HashMap<String, HashMap<String, String>> nffgNodeIds = new HashMap<>();
	
	// A map that holds each N4J Node Id for loaded hosts
	private HashMap<String, String> hostIds = new HashMap<>();
	
	public Neo4JSimpleXMLInterface(URI baseURI) throws Neo4JException {
		client = ClientBuilder.newClient();
		dataTarget = client.target(baseURI).path("data");
		
		try {
			String neo4jSchemaString = client.target(baseURI)
											 .path(N4J_SCHEMA_PATH)
											 .request()
											 .get(String.class);
											 
			StreamSource schemaSource = new StreamSource(new StringReader(neo4jSchemaString));
			
			context = JAXBContext.newInstance("it.polito.dp2.NFV.sol3.service.neo4j");
			
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(schemaSource);
			validator = schema.newValidator();
		}
		catch (JAXBException je) { throw new Neo4JException("Error creating JAXB Context"); } 
		catch (SAXException e) { throw new Neo4JException("Error creating Neo4J Schema validator"); }
	}
	
	public void postNFFG(NFFG nffg) throws Neo4JException {
		try  {
			HashMap<String, String> nodeIds = new HashMap<>();
			nffgNodeIds.put(nffg.getName(), nodeIds);
			
			// Load each node in the NFFG and eventually their Hosts. Also link nodes to hosts.
			for (Node nt :nffg.getNode()) {
				postNffgNode(nt, nodeIds);
			}
			
			// Once all nodes are loaded onto the system, post forwardsTo relationships
			for (Node nt : nffg.getNode()) {
				String sourceId = nodeIds.get(nt.getName());
				
				// Exceptional case, should not happen
				if (sourceId == null) { 
					throw new Neo4JException("Node " + nt.getName() + " was loaded but its Id was not found"); 
				}
				
				for (Link lt : nt.getLink()) {
					String dest = lt.getDest();
					String destId;
					
					// Exceptional case, should not happen
					if (dest == null || (destId = nodeIds.get(dest)) == null) {
						throw new Neo4JException("Node " + nt.getName() + " forwards to an invalid node");
					}
					
					forwardsTo(sourceId, destId);
				}
			}
		}
		catch (Exception e) {
			// Clean local state
			nffgNodeIds.remove(nffg.getName());
			throw new Neo4JException("An unexpected error occurred", e);
		}
	}

	/**
	 * Posts a "forwardTo" relationship.
	 * 
	 * @param sourceId source NFFG node id
	 * @param destId destination NFFG node id
	 */
	private void forwardsTo(String sourceId, String destId) throws Neo4JException {
		try {
			Relationship rel = new Relationship();
			rel.setDstNode(destId);
			rel.setType("ForwardsTo");
			
			dataTarget.path("node")
				  .path(sourceId)
				  .path("relationships")
				  .request()
				  .post(Entity.entity(rel, MediaType.APPLICATION_XML_TYPE), Relationship.class);
		} 
		catch (Exception e) {
			throw new Neo4JException("An unexpected error occurred.", e);
		}
	}

	public void forwardsTo(String srcNodeName, String dstNodeName, String nffgName) throws Neo4JException {
		HashMap<String, String> nodeIds = nffgNodeIds.get(nffgName);
		if (nodeIds == null) { throw new Neo4JException("Neo4J: No Id Map for Nffg " + nffgName); }
		
		String srcId, dstId;
		srcId = nodeIds.get(srcNodeName);
		dstId = nodeIds.get(dstNodeName);
		if (srcId == null || dstId == null) { throw new Neo4JException("Neo4J: Node Ids not found"); }
		
		forwardsTo(srcId, dstId);
	}
	
	/**
	 * Posts a "allocatedOn" relationship
	 * 
	 * @param nffgNodeId hosted NFFG node id
	 * @param hostNodeId hosting Host node id
	 */
	private void allocatedOn(String nffgNodeId, String hostNodeId) throws Neo4JException {
		try {
			Relationship rel = new Relationship();
			rel.setDstNode(hostNodeId);
			rel.setType("AllocatedOn");
			
			dataTarget.path("node")
				  .path(nffgNodeId)
				  .path("relationships")
				  .request()
				  .post(Entity.entity(rel, MediaType.APPLICATION_XML_TYPE), Relationship.class);
		} 
		catch (Exception e) {
			throw new Neo4JException("An unexpected error occurred", e);
		}
	}

	/**
	 * Posts a new Host node and save its ID
	 * 
	 * @param hr the source host reader
	 * @return the N4J node id for the posted node
	 */
	public String postHost(Host ht) throws Neo4JException {
		try {
			it.polito.dp2.NFV.sol3.service.neo4j.Node n4jNode = new it.polito.dp2.NFV.sol3.service.neo4j.Node();
			Labels ls = new Labels();
			Properties ps = new Properties();
			Property p = new Property();
			
			ps.getProperty().add(p);
			p.setName("name");
			p.setValue(ht.getName());
			
			n4jNode.setProperties(ps);
			
			// Post Host node
			it.polito.dp2.NFV.sol3.service.neo4j.Node response =
							dataTarget.path("node")
								  .request(MediaType.APPLICATION_XML_TYPE)
								  .post(Entity.entity(n4jNode, MediaType.APPLICATION_XML_TYPE), it.polito.dp2.NFV.sol3.service.neo4j.Node.class);
			
			JAXBSource nodeSource = new JAXBSource(context, response);
			validator.validate(nodeSource);
			
			// Post Host label
			ls.getLabel().add("Host");
			dataTarget.path("node")
				  .path(response.getId())
				  .path("labels")
				  .request(MediaType.APPLICATION_XML_TYPE)
				  .post(Entity.entity(ls, MediaType.APPLICATION_XML_TYPE), String.class);
			
			hostIds.put(ht.getName(), response.getId());
			return response.getId();
		}
		catch (Exception e) {
			hostIds.remove(ht.getName());
			throw new Neo4JException("An unexpected error occurred", e);			
		}
	}

	/**
	 * Post a new NFFG Node node and save its id.
	 * 
	 * @param nt the source node reader
	 * @param idsMap the map where to save the id.
	 * @return the N4J node id for the posted node
	 * @throws ServiceException 
	 */
	private void postNffgNode(Node nt, HashMap<String, String> idsMap) throws Neo4JException {
		try {
			it.polito.dp2.NFV.sol3.service.neo4j.Node n4jNode = new it.polito.dp2.NFV.sol3.service.neo4j.Node();
			Labels ls = new Labels();
			Properties ps = new Properties();
			Property p = new Property();
			
			ps.getProperty().add(p);
			p.setName("name");
			p.setValue(nt.getName());
			n4jNode.setProperties(ps);
			
			// Post NFFG Node
			it.polito.dp2.NFV.sol3.service.neo4j.Node response =
							dataTarget.path("node")
								  .request(MediaType.APPLICATION_XML_TYPE)
								  .post(Entity.entity(n4jNode, MediaType.APPLICATION_XML_TYPE), it.polito.dp2.NFV.sol3.service.neo4j.Node.class);
			
			JAXBSource nodeSource = new JAXBSource(context, response);
			validator.validate(nodeSource);
			
			// Post Node label
			ls.getLabel().add("Node");
			dataTarget.path("node")
				  .path(response.getId())
				  .path("labels")
				  .request(MediaType.APPLICATION_XML_TYPE)
				  .post(Entity.entity(ls, MediaType.APPLICATION_XML_TYPE), String.class);
			
			idsMap.put(nt.getName(), response.getId());
			
			String hostNodeId = hostIds.get(nt.getHost());
			String nffgNodeId = response.getId();
			allocatedOn(nffgNodeId, hostNodeId);
		} 
		catch (Exception e) {
			idsMap.remove(nt.getName());
			throw new Neo4JException("An unexpected error occurred", e);
		}
	}
	
	public void postNffgNode(Node nt, String nffgName) throws Neo4JException {
		HashMap<String, String> nodeIds = nffgNodeIds.get(nffgName);
		if (nodeIds == null) { throw new Neo4JException("Neo4J: No Id Map for Nffg " + nffgName); }
		else { postNffgNode(nt, nodeIds); }
	}

	public Set<String> getReachableHosts(String nffgName, String nodeName) throws Neo4JException {
		HashMap<String, String> nodeIds = nffgNodeIds.get(nffgName);
		if (nodeIds == null) { throw new Neo4JException("Unknown Nffg name: " + nffgName); }
		
		String nodeId = nodeIds.get(nodeName);
		if (nodeId == null) { throw new Neo4JException("Unknown Node name: " + nodeName); }
		
		try {
			Nodes ns = dataTarget.path("node")
							 .path(nodeId)
							 .path("reachableNodes")
							 .queryParam("nodeLabel", "Host")
							 .request(MediaType.APPLICATION_XML_TYPE)
							 .get(Nodes.class);
			
			JAXBSource nodeSource = new JAXBSource(context, ns);
			validator.validate(nodeSource);

			return ns.getNode().stream()
							   // Extract not-null properties named "name"
							   .map(n -> 
							   		n.getProperties().getProperty().stream()
							   									   .filter(p -> p.getName().equals("name"))
									   							   .findFirst()
									   							   .orElse(null)
				 			   )
							   .filter(p -> p != null)
							   // Collect matching host readers
							   .map(p -> p.getValue())
							   .collect(Collectors.toSet());
		}
		catch (Exception e) {
			throw new Neo4JException("An unexpected error occurred", e);
		}
	}

}
