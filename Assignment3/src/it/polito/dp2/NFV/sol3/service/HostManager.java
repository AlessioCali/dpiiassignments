package it.polito.dp2.NFV.sol3.service;

import java.util.ArrayList;

import it.polito.dp2.NFV.sol3.service.jaxb.Host;
import it.polito.dp2.NFV.sol3.service.jaxb.NAType;
import it.polito.dp2.NFV.sol3.service.jaxb.VNFType;

/**
 * This class is used to encapsulate a Host and manage
 * allocations on hosts. To ease the deployment of multiple nodes,
 * while at the same time offering the possibility to roll back
 * the allocation, an allocation queue keeps track of all nodes
 * pending allocation. Subsequent checks to the host availability
 * in terms of storage and memory are made against the types stored 
 * in the queue. The queue can be committed by calling {@link #allocate()}
 * or flushed by calling {@link #clearQueue()}. Flushing the queue without
 * committing causes the allocation process to be aborted (this can be used,
 * for example, to recover an Nffg deployment which fails due to lack
 * of resources). 
 * 
 * @author Alessio
 *
 */
public class HostManager {
	
	private final Host host;
	
	private int memoryLeft = 0;
	private int storageLeft = 0;
	private int allocatedVNFs = 0;
	
	private int queuedMemory = 0;
	private int queuedStorage = 0;
	private ArrayList<NAType> allocationQueue = new ArrayList<>();
	
	public HostManager(Host host) {
		this.host = host;
		this.memoryLeft = host.getMemoryMB();
		this.storageLeft = host.getStorageMB();
		this.allocatedVNFs = host.getAllocations().getNodeAllocation().size();
	}

	/**
	 * Tells whether it's possible to queue another node for allocation.
	 * The check is done against already allocated nodes and nodes pending
	 * allocation.
	 * 
	 * @param vnf The VNF of the node to allocate.
	 * @return True if a node of the given VNF can be allocated, false otherwise.
	 */
	public boolean canAllocate(VNFType vnf) {
		return allocatedVNFs < host.getMaxVNF() + allocationQueue.size() &&
			   storageLeft >= vnf.getStorageMB() + queuedStorage &&
			   memoryLeft >= vnf.getMemoryMB() + queuedMemory;
	}
	
	public Host getHost() { return host; }

	/**
	 * Queues a node for allocation. The node won't be committed
	 * until a call to {@link #allocate()} is made.
	 * 
	 * @param na The node allocation to queue.
	 * @param vnf The VNF of the node to allocate.
	 */
	public void queue(NAType na, VNFType vnf) {
		queuedMemory += vnf.getMemoryMB();
		queuedStorage += vnf.getStorageMB();
		allocationQueue.add(na);
	}
	
	/**
	 * Commits the allocation queue, thus committing all pending
	 * nodes. The allocation queue is flushed after this operation. 
	 */
	public void allocate() {
		memoryLeft -= queuedMemory;
		storageLeft -= queuedStorage;
		allocatedVNFs += allocationQueue.size();
		host.getAllocations().getNodeAllocation().addAll(allocationQueue);
		clearQueue();
	}
	
	/**
	 * Flushes the allocation queue. The host changes are not confirmed
	 * thus aborting allocation. 
	 */
	public void clearQueue() {
		queuedMemory = queuedStorage = 0;
		allocationQueue.clear();
	}
}
