package it.polito.dp2.NFV.sol3.service;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import it.polito.dp2.NFV.sol3.service.jaxb.Allocations;
import it.polito.dp2.NFV.sol3.service.jaxb.Connections;
import it.polito.dp2.NFV.sol3.service.jaxb.Host;
import it.polito.dp2.NFV.sol3.service.jaxb.HostRefs;

@Path("hosts")
public class HostsResource {
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public HostRefs getHosts() {
		try {
			return BackendResources.getSingleton().getHostRefs();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}

	@GET
	@Path("{hostName}")
	@Produces(MediaType.APPLICATION_XML)
	public Host getHostByName(@PathParam("hostName") String hostName) {
		try {
			Host ht = BackendResources.getSingleton().getHost(hostName);
			return ht;
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@GET
	@Path("{hostName}/nodes")
	@Produces(MediaType.APPLICATION_XML)
	public Allocations getAllocatedNodes(@PathParam("hostName") String hostName) {
		try {
			Allocations as = BackendResources.getSingleton().getAllocations(hostName);
			return as;
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
	
	@GET
	@Path("{hostName}/connections")
	@Produces(MediaType.APPLICATION_XML)
	public Connections getConnections(@PathParam("hostName") String hostName,
													   @QueryParam("dest") String dest) {
		try {
			Connections ct = BackendResources.getSingleton().getConnections(hostName, dest);
			return ct;
		}
		catch (UnknownEntityException ue) { throw new NotFoundException(); }
		catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException();
		}
	}
}
