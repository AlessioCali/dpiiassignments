package it.polito.dp2.NFV.sol3.client2;

import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Validator;

import it.polito.dp2.NFV.ConnectionPerformanceReader;
import it.polito.dp2.NFV.FunctionalType;
import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.sol3.client2.nfvdeployer.*;

class NfvReaderImpl implements NfvReader {
	
	private WebTarget serviceTarget;
	
	private VNFCatalog vnfc;
	private HashMap<String, Host> hosts = new HashMap<>();
	private HashMap<String, NFFG> nffgs = new HashMap<>();
	
	private JAXBContext context;
	private Validator validator;
	
	private class UnavailableResourceException extends Exception {
		
		private static final long serialVersionUID = 6779429658441652086L;

		public UnavailableResourceException(String message) { super(message); }

		public UnavailableResourceException(String msg, Throwable cause) { super(msg, cause); }
		
	}
	
	private class ResourceNotFoundException extends Exception {

		private static final long serialVersionUID = 6197434971909496123L;

		public ResourceNotFoundException(String msg) { super(msg); }
		
	}
	
	private class ResourceValidationException extends Exception {

		private static final long serialVersionUID = -8281104219300358857L;

		public ResourceValidationException(Throwable cause) { super(cause); }
		
	}
	
	private class ConnectionPerformanceReaderImpl implements ConnectionPerformanceReader {

		private CPType cp;
		
		protected ConnectionPerformanceReaderImpl(CPType cp) { this.cp = cp; }
		
		@Override
		public int getLatency() { return cp.getLatency(); }

		@Override
		public float getThroughput() { return cp.getThroughput(); }
		
	}
	
	private class HostReaderImpl implements HostReader {
		
		private Host ht;
		
		protected HostReaderImpl(Host ht) { this.ht = ht; }

		@Override
		public String getName() { return ht.getName(); }

		@Override
		public int getAvailableMemory() { return ht.getMemoryMB(); }

		@Override
		public int getAvailableStorage() { return ht.getStorageMB(); }

		@Override
		public int getMaxVNFs() { return ht.getMaxVNF(); }

		@Override
		public Set<NodeReader> getNodes() { 
			return ht.getAllocations()
					 .getNodeAllocation()
					 .stream()
					 .filter(nat -> nffgs.containsKey(nat.getNffg()))	// Filter only valid Nffgs
					 .map(nat -> {
					 		NFFG nffg = nffgs.get(nat.getNffg());	// Get the correct node
					 		Node nt = nffg.getNode()
					 						  .stream()
					 						  .filter(nt2 -> nt2.getName().equals(nat.getNode()))
					 						  .findFirst()
					 						  .orElse(null);
					 		
					 		if (nt == null) { return null; }			// Return null if node is invalid somehow
					 		else { return new NodeReaderImpl(nt, nffg.getName()); }
					 	}
					 )
					 .filter(nri -> nri != null)	// Remove invalid cases
					 .collect(Collectors.toSet());
		}
		
	}
	
	private class NodeReaderImpl implements NodeReader {
		
		private Node nt;
		private String nffgName;
		
		protected NodeReaderImpl(Node nt, String nffgName) { 
			this.nt = nt;
			this.nffgName = nffgName;
		}

		@Override
		public String getName() { return nt.getName(); }

		@Override
		public VNFTypeReader getFuncType() {
			VNFType vnf = vnfc.getVNF().stream()
						 			   .filter(vnft -> vnft.getName().equals(nt.getVnf()))
						 			   .findFirst()
						 			   .orElse(null);
			
			return new VNFTypeReaderImpl(vnf);
		}

		@Override
		public HostReader getHost() { return new HostReaderImpl(hosts.get(nt.getHost())); }

		@Override
		public Set<LinkReader> getLinks() {
			return nt.getLink().stream()
							   .map(lt -> new LinkReaderImpl(lt, nffgName, nt.getName()))
							   .collect(Collectors.toSet());
		}

		@Override
		public NffgReader getNffg() { return new NffgReaderImpl(nffgs.get(nffgName)); }
		
		
	}
	
	private class NffgReaderImpl implements NffgReader {
		
		private NFFG nffgt;
		
		protected NffgReaderImpl(NFFG nffgt) { this.nffgt = nffgt; }

		@Override
		public String getName() { return nffgt.getName(); }

		@Override
		public Calendar getDeployTime() { return nffgt.getDeployTime().toGregorianCalendar(); }

		@Override
		public NodeReader getNode(String nodeName) {
			Node nt = nffgt.getNode().stream()
										 .filter(node -> node.getName().equals(nodeName))
										 .findFirst()
										 .orElse(null);
			
			if (nt == null) { return null; }
			else { return new NodeReaderImpl(nt, nffgt.getName()); }
		}

		@Override
		public Set<NodeReader> getNodes() {
			return nffgt.getNode().stream()
								  .map(node -> new NodeReaderImpl(node, nffgt.getName()))
								  .collect(Collectors.toSet());
		}
		
	}
	
	private class LinkReaderImpl implements LinkReader {
		
		private Link lt;
		private String nffgName;
		private String srcNodeName;
		
		protected LinkReaderImpl(Link lt, String nffgName, String srcNodeName) {
			this.lt = lt;
			this.nffgName = nffgName;
			this.srcNodeName = srcNodeName;
		}

		@Override
		public String getName() { return lt.getName(); }

		@Override
		public NodeReader getDestinationNode() {
			NFFG nffg = nffgs.get(nffgName);
			Node dst = nffg.getNode().stream()
										 .filter(nt -> nt.getName().equals(lt.getDest()))
										 .findFirst()
										 .orElse(null);
			
			return new NodeReaderImpl(dst, nffgName);
		}

		@Override
		public int getLatency() { return lt.getLatency(); }

		@Override
		public NodeReader getSourceNode() {
			NFFG nffg = nffgs.get(nffgName);
			Node src = nffg.getNode().stream()
										 .filter(nt -> nt.getName().equals(srcNodeName))
										 .findFirst()
										 .orElse(null);
			
			return new NodeReaderImpl(src, nffgName);
		}

		@Override
		public float getThroughput() { return lt.getThroughput(); }		
		
	}
	
	private class VNFTypeReaderImpl implements VNFTypeReader {
		
		private VNFType vnft;
		
		protected VNFTypeReaderImpl(VNFType vnft) { this.vnft = vnft; }

		@Override
		public String getName() { return vnft.getName(); }

		@Override
		public FunctionalType getFunctionalType() {
			return FunctionalType.fromValue(vnft.getType().name());
		}

		@Override
		public int getRequiredMemory() { return vnft.getMemoryMB(); }

		@Override
		public int getRequiredStorage() { return vnft.getStorageMB(); }
		
		
	}
	
	protected NfvReaderImpl (URI baseUri, JAXBContext context, Validator validator) throws UnavailableResourceException, ResourceNotFoundException, ResourceValidationException {
		Client client = ClientBuilder.newClient();
		serviceTarget = client.target(baseUri);
		
		this.context = context;
		this.validator = validator;
		
		fetchCatalog();
		fetchHosts();
		fetchNffgs();	
	}

	private void fetchCatalog() throws UnavailableResourceException, ResourceNotFoundException, ResourceValidationException {
		WebTarget vnfTarget = serviceTarget.path("vnfs");
		vnfc = getRemoteResource(vnfTarget, VNFCatalog.class);
		validate(vnfc);
	}
	
	private void fetchHosts() throws UnavailableResourceException, ResourceNotFoundException, ResourceValidationException {
		WebTarget hostRefsTarget = serviceTarget.path("hosts");
		HostRefs hostRefs = getRemoteResource(hostRefsTarget, HostRefs.class);
		validate(hostRefs);
		
		for (ObjectReference ref : hostRefs.getHostRef()) {
			WebTarget hostTarget = serviceTarget.path(ref.getHref());
			Host ht = getRemoteResource(hostTarget, Host.class);
			validate(ht);
			hosts.put(ht.getName(), ht);
		}
	}
	
	private void fetchNffgs() throws UnavailableResourceException, ResourceNotFoundException, ResourceValidationException {
		WebTarget nffgRefsTarget = serviceTarget.path("nffgs");
		NFFGRefs nffgRefs = getRemoteResource(nffgRefsTarget, NFFGRefs.class);
		validate(nffgRefs);
		
		for (NFFGRef nffgRef : nffgRefs.getNFFGRef()) {
			WebTarget nffgTarget = serviceTarget.path(nffgRef.getHref());
			NFFG nffg = getRemoteResource(nffgTarget, NFFG.class);
			validate(nffg);
			nffgs.put(nffg.getName(), nffg);
		}
	}

	/**
	 * Tries to fetch a remote resource from the given path, generating exceptions
	 * if this is not possible.
	 * 
	 * @param target The web target of the resource to get.
	 * @param type The class to deserialize.
	 * @param params (Name, Value) pairs of query parameters. Spurious elements are ignored.
	 * @return The desired resource
	 * @throws ResourceNotFoundException if the given resource could not be found.
	 * @throws UnavailableResourceException if the given resource cannot be reached.
	 */
	private <T> T getRemoteResource (WebTarget target, Class<? extends T> type, String... params) throws UnavailableResourceException, ResourceNotFoundException {
		Response r;
		int i = 0;
		
		while (i < params.length - 1) {
			target = target.queryParam(params[i], params[i + 1]);
			i += 2;
		}
		
		try {
			r = target.request(MediaType.APPLICATION_XML).get();
		}
		catch (ProcessingException pe) { throw new UnavailableResourceException("An error occurred while processing the server response.", pe); }
		
		if (r.getStatusInfo() != Response.Status.OK) {
			
			if (r.getStatusInfo() == Response.Status.NOT_FOUND) {
				throw new ResourceNotFoundException("Resource not found in path " + target.getUri().toString());
			}
			else {
				throw new UnavailableResourceException(
						"Fetching resource failed: \n" + 
						"HTTP " + r.getStatus() + "\n" +
						r.getStatusInfo().getReasonPhrase()
				); 				
			}
			
		}
		else { return r.readEntity(type); }
	}
	
	/**
	 * Validates an object using this reader's schema and context-
	 * 
	 * @param o The object to validate.
	 * @throws ResourceValidationException if validation fails.
	 */
	private void validate(Object o) throws ResourceValidationException {
		try {
			JAXBSource src = new JAXBSource(context, o);
			validator.validate(src);
		}
		catch (Exception e) { throw new ResourceValidationException(e); }
	}

	@Override
	public ConnectionPerformanceReader getConnectionPerformance(HostReader from, HostReader to) {
		Host fromType = hosts.get(from.getName());
		if (fromType == null) { return null; }
		
		CPType cp = fromType.getConnections().getConnection()
											 .stream()
											 .filter(cpt -> cpt.getTo().equals(to.getName()))
											 .findFirst()
											 .orElse(null);
		
		if (cp == null) { return null; }
		else { return new ConnectionPerformanceReaderImpl (cp); }
	}

	@Override
	public HostReader getHost(String hostName) {
		Host ht = hosts.get(hostName);
		if (ht == null) { return null; }
		else { return new HostReaderImpl(ht); }
	}

	@Override
	public Set<HostReader> getHosts() {
		return hosts.values().stream()
							 .map(ht -> new HostReaderImpl(ht))
							 .collect(Collectors.toSet());
	}

	@Override
	public NffgReader getNffg(String nffgName) {
		NFFG nffg = nffgs.get(nffgName);
		if (nffg == null) { return null; }
		else { return new NffgReaderImpl(nffg); }
	}

	@Override
	public Set<NffgReader> getNffgs(Calendar since) {
		Stream<NFFG> nffgStream = nffgs.values().stream();
		
		if (since != null) { 
			nffgStream = nffgStream.filter(nffg -> nffg.getDeployTime().toGregorianCalendar().after(since)); 
		}
		
		return nffgStream.map(nffg -> new NffgReaderImpl(nffg))
						 .collect(Collectors.toSet());
	}

	@Override
	public Set<VNFTypeReader> getVNFCatalog() {
		return vnfc.getVNF().stream()
							.map(vnf -> new VNFTypeReaderImpl(vnf))
							.collect(Collectors.toSet());
	}
}
