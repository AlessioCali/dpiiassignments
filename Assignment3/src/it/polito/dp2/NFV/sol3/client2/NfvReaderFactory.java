package it.polito.dp2.NFV.sol3.client2;

import java.io.StringReader;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NfvReaderException;

public class NfvReaderFactory extends it.polito.dp2.NFV.NfvReaderFactory {

	private static final String SERVICE_SCHEMA_LOCATION = "application.wadl/xsd0.xsd";
	
	@Override
	public NfvReader newNfvReader() throws NfvReaderException {
		try {
			String baseUriString = System.getProperty("it.polito.dp2.NFV.lab3.URL", "http://localhost:8080/NfvDeployer/rest/");
			
			JAXBContext context = JAXBContext.newInstance("it.polito.dp2.NFV.sol3.client2.nfvdeployer");
			
			Client client = ClientBuilder.newClient();
			String schemaString = client.target(baseUriString)
										.path(SERVICE_SCHEMA_LOCATION)
										.request()
										.get(String.class);
			
			StreamSource schemaSource = new StreamSource(new StringReader(schemaString));
			
			SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(schemaSource);
			Validator validator = schema.newValidator();
			
			return new NfvReaderImpl(UriBuilder.fromUri(baseUriString).build(), context, validator);
		}
		catch (Exception e) {
			throw new NfvReaderException(e, "An internal error occurred");
		}
	}

}
