package it.polito.dp2.NFV.sol3.client1;

import javax.ws.rs.core.UriBuilder;

import it.polito.dp2.NFV.lab3.NfvClient;
import it.polito.dp2.NFV.lab3.NfvClientException;

public class NfvClientFactory extends it.polito.dp2.NFV.lab3.NfvClientFactory {

	@Override
	public NfvClient newNfvClient() throws NfvClientException {
		try {
			String baseUri = System.getProperty("it.polito.dp2.NFV.lab3.URL", "http://localhost:8080/NfvDeployer/rest/");
			return new NfvClientImpl(UriBuilder.fromUri(baseUri).build());
		}
		catch (Exception e) {
			throw new NfvClientException(e, "An internal error occurred");
		}
	}

}
