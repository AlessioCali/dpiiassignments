package it.polito.dp2.NFV.sol3.client1;

public class ResourceValidationException extends Exception {

	private static final long serialVersionUID = -547409829938819966L;

	public ResourceValidationException() { }

	public ResourceValidationException(String arg0) {
		super(arg0);
	}

	public ResourceValidationException(Throwable arg0) {
		super(arg0);
	}

	public ResourceValidationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ResourceValidationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
