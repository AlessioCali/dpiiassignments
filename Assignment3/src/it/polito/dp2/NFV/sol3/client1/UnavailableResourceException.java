package it.polito.dp2.NFV.sol3.client1;

public class UnavailableResourceException extends Exception {
		
	private static final long serialVersionUID = 6779429658441652086L;

	public UnavailableResourceException(String message) { super(message); }
	
	public UnavailableResourceException(String message, Throwable cause) { super(message, cause); }

	public UnavailableResourceException(Throwable cause) { super(cause); }

}
