package it.polito.dp2.NFV.sol3.client1;

import java.io.StringReader;
import java.net.URI;
import java.util.HashMap;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.RandomStringUtils;
import org.xml.sax.SAXException;

import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.lab3.AllocationException;
import it.polito.dp2.NFV.lab3.DeployedNffg;
import it.polito.dp2.NFV.lab3.LinkAlreadyPresentException;
import it.polito.dp2.NFV.lab3.LinkDescriptor;
import it.polito.dp2.NFV.lab3.NffgDescriptor;
import it.polito.dp2.NFV.lab3.NfvClient;
import it.polito.dp2.NFV.lab3.NoNodeException;
import it.polito.dp2.NFV.lab3.NodeDescriptor;
import it.polito.dp2.NFV.lab3.ServiceException;
import it.polito.dp2.NFV.lab3.UnknownEntityException;
import it.polito.dp2.NFV.sol3.client1.nfvdeployer.Link;
import it.polito.dp2.NFV.sol3.client1.nfvdeployer.NFFG;
import it.polito.dp2.NFV.sol3.client1.nfvdeployer.NFFGRef;
import it.polito.dp2.NFV.sol3.client1.nfvdeployer.Node;

public class NfvClientImpl implements NfvClient {
	
	private static final String SERVICE_SCHEMA_LOCATION = "application.wadl/xsd0.xsd";
	
	private JAXBContext context;
	private Validator validator;

	private WebTarget serviceTarget;
	
	private static final String randPrefix = RandomStringUtils.randomAlphabetic(1) + 
											 RandomStringUtils.randomAlphanumeric(10);
	
	private static int nffgCounter = 0;
	private static int nodeCounter = 0;
	private static int linkCounter = 0;
	
	private class DeployedNffgImpl implements DeployedNffg {
		
		private WebTarget nffgTarget;
		private String nffgName;
		
		protected DeployedNffgImpl(String nffgPath, String nffgName) { 
			this.nffgTarget = serviceTarget.path(nffgPath);
			this.nffgName = nffgName;
		}

		@Override
		public NodeReader addNode(VNFTypeReader type, String hostName) throws AllocationException, ServiceException {
			Node nt = new Node();
			nt.setName(randPrefix + type.getName() + nodeCounter++);
			nt.setHost(hostName);
			nt.setVnf(type.getName());
			
			try {
				Response response = nffgTarget.path("nodes")
											 .request()
											 .post(Entity.entity(nt, MediaType.APPLICATION_XML_TYPE));
				
				if (response.getStatus() != 201) { 
					if (response.getStatus() == 507) { throw new AllocationException("Server returned status code 507. Allocation failed."); }
					else { throw new ServiceException("Server returned an unexpected status code: " + response.getStatus()); }
				}
			}
			catch (AllocationException | ServiceException e) { throw e; }
			catch (Exception e) {
				throw new ServiceException("An unexpected error occured.", e);
			}
			
			NfvReaderImpl nfvr;
			try {
				 nfvr = new NfvReaderImpl(serviceTarget, context, validator);
			} catch (UnavailableResourceException | ResourceNotFoundException | ResourceValidationException e) {
				throw new ServiceException("Impossible to read NFV data after node deployment.", e);
			}
			
			NffgReader nffg = nfvr.getNffg(nffgName);
			if (nffg == null) { throw new ServiceException("Node deployment succeeded but could not find Nffg in server data."); }
			
			NodeReader node = nffg.getNode(nt.getName());
			if (node == null) { throw new ServiceException("Node deployment succeeded but could not find Node in server data."); }
			
			return node;
		}

		@Override
		public LinkReader addLink(NodeReader source, NodeReader dest, boolean overwrite) throws NoNodeException, LinkAlreadyPresentException, ServiceException {
			Link lt = new Link();
			lt.setDest(dest.getName());
			lt.setName(randPrefix + "Link" + linkCounter++);
			
			try {
				Response response =	nffgTarget.path("nodes")
											  .path(source.getName())
											  .path("links")
											  .queryParam("overwrite", String.valueOf(overwrite))
											  .request()
											  .post(Entity.entity(lt, MediaType.APPLICATION_XML_TYPE));
				
				if (response.getStatus() != 201) { 
					switch (response.getStatus()) {
					case 404:
						throw new NoNodeException("Server returned status code 404: No Node (or Nffg) with that name was found.");
					case 409:
						throw new LinkAlreadyPresentException("Server returned status code 409: A link conflict arised.");
					default:
						throw new ServiceException("Server returned status code: " + response.getStatus());
					}
				}
			}
			catch (NoNodeException | LinkAlreadyPresentException | ServiceException e) { throw e; }
			catch (Exception e) {
				throw new ServiceException("An unexpected error occurred.", e);
			}
			
			NfvReaderImpl nfvr;
			
			try {
				nfvr = new NfvReaderImpl(serviceTarget, context, validator);
			} catch (UnavailableResourceException | ResourceNotFoundException | ResourceValidationException e) {
				throw new ServiceException("An error occurred while trying to get Nfv data after link deployment.", e);
			}
			
			NffgReader nffg = nfvr.getNffg(nffgName);
			if (nffg == null) { throw new ServiceException("Link deployment succeeded but Nffg was not found in server data."); }
			
			NodeReader node = nffg.getNode(source.getName());
			if (node == null) { throw new ServiceException("Link deployment succeeded but Node was not found in server data."); }
			
			LinkReader link = node.getLinks()
								   .stream()
								   .filter(lr -> lr.getName().equals(lt.getName()))
								   .findFirst()
								   .orElse(null);
			
			if (link == null) { throw new ServiceException("Link deployment succeeded but Link was not found in server data."); }
			return link;
		}
		
		@Override
		public NffgReader getReader() throws ServiceException {
			NfvReaderImpl nfvr;
			
			try {
				nfvr = new NfvReaderImpl(serviceTarget, context, validator);
			} catch (UnavailableResourceException | ResourceNotFoundException | ResourceValidationException e) {
				throw new ServiceException(e);
			}
			
			NffgReader nffg = nfvr.getNffg(nffgName);
			if (nffg == null) { throw new ServiceException("Nffg deployment succeeded but Nffg was not found in server data."); }
			return nffg;
		}
	}
	
	protected NfvClientImpl(URI baseUri) throws JAXBException, SAXException {
		Client client = ClientBuilder.newClient();
		serviceTarget = client.target(baseUri);
		
		context = JAXBContext.newInstance("it.polito.dp2.NFV.sol3.client1.nfvdeployer");
		
		String schemaString = serviceTarget.path(SERVICE_SCHEMA_LOCATION)
										   .request()
										   .get(String.class);
		
		StreamSource schemaSource = new StreamSource(new StringReader(schemaString));
		
		SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = sf.newSchema(schemaSource);
		validator = schema.newValidator();
	}
	
	@Override
	public DeployedNffg deployNffg(NffgDescriptor nffg) throws AllocationException, ServiceException {
		WebTarget nffgTarget = serviceTarget.path("nffgs");
		NFFG nffgEntity = new NFFG();
		NFFGRef ref;
		
		nffgEntity.setName(randPrefix + "Nffg" + nffgCounter++);
		
		// This map is used to track descriptor-node associations.
		// It will be used later to get destination node names for links. 
		HashMap<NodeDescriptor, Node> nodeMap = new HashMap<>();
		
		for (NodeDescriptor nd : nffg.getNodes()) {
			Node nt = new Node();
			nt.setHost(nd.getHostName());
			nt.setName(randPrefix + nd.getFuncType().getName() + nodeCounter++);
			nt.setVnf(nd.getFuncType().getName());
			
			nffgEntity.getNode().add(nt);
			nodeMap.put(nd, nt);
		}
		
		for (NodeDescriptor nd : nffg.getNodes()) {
			for (LinkDescriptor ld : nd.getLinks()) {
				Node dstNode = nodeMap.get(ld.getDestinationNode());
				Node srcNode = nodeMap.get(ld.getSourceNode());
				
				// This can happen only if the representation is inconsistent...
				if (dstNode == null || srcNode == null) { 
					throw new ServiceException("Representation is inconsistent: Referenced nodes in a Link were not found."); 
				}
				else {
					Link lt = new Link();
					lt.setDest(dstNode.getName());
					lt.setLatency(ld.getLatency());
					lt.setName(randPrefix + "Link" + linkCounter++);
					lt.setThroughput(ld.getThroughput());
					srcNode.getLink().add(lt);
				}
			}
		}
		
		try {
			Response res = nffgTarget.request(MediaType.APPLICATION_XML)
					  				 .post(Entity.entity(nffgEntity, MediaType.APPLICATION_XML));
			
			if (res.getStatus() != 201) {
				if (res.getStatus() == 507) { throw new AllocationException("Server returned status code 507: Allocation failed."); }
				else { throw new ServiceException("Unexpected server status code: " + res.getStatus()); }
			}
			
			ref = res.readEntity(NFFGRef.class);
			validate(ref);
		}
		catch (AllocationException | ServiceException e) { throw e; }
		catch (Exception e) {
			throw new ServiceException("An unexpected exception was thrown while contacting the server.", e);
		}
		
		return new DeployedNffgImpl(ref.getHref(), nffgEntity.getName());
	}

	@Override
	public DeployedNffg getDeployedNffg(String name) throws UnknownEntityException, ServiceException {
		String nffgPath = UriBuilder.fromPath("nffgs").path(name).build().toASCIIString();
		
		// Check if remote resource exists
		try {
			NFFG nffg = getRemoteResource(serviceTarget.path(nffgPath), NFFG.class);
			validate(nffg);
		}
		catch (ResourceNotFoundException rnf) {
			throw new UnknownEntityException("No deployed Nffg with name " + name + " was found.");
		}
		catch (Exception e) {
			throw new ServiceException("An error occurred while contacting the server.", e);
		}
		
		return new DeployedNffgImpl(nffgPath, name);
	}
	
	/**
	 * Tries to fetch a remote resource from the given path, generating exceptions
	 * if this is not possible.
	 * 
	 * @param target The web target of the resource to get.
	 * @param type The class to deserialize.
	 * @param params (Name, Value) pairs of query parameters. Spurious elements are ignored.
	 * @return The desired resource
	 * @throws ResourceNotFoundException if the given resource could not be found.
	 * @throws UnavailableResourceException if the given resource cannot be reached.
	 */
	private <T> T getRemoteResource (WebTarget target, Class<? extends T> type, String... params) throws UnavailableResourceException, ResourceNotFoundException {
		Response r;
		int i = 0;
		
		while (i < params.length - 1) {
			target = target.queryParam(params[i], params[i + 1]);
			i += 2;
		}
		
		try {
			r = target.request(MediaType.APPLICATION_XML).get();
		}
		catch (ProcessingException pe) { throw new UnavailableResourceException("An error occurred while processing the server response.", pe); }
		
		if (r.getStatusInfo() != Response.Status.OK) {
			
			if (r.getStatusInfo() == Response.Status.NOT_FOUND) {
				throw new ResourceNotFoundException("Resource not found at path " + target.getUri().toString());
			}
			else {
				throw new UnavailableResourceException(
						"Fetching resource failed: \n" + 
						"HTTP " + r.getStatus() + "\n" +
						r.getStatusInfo().getReasonPhrase()
				); 				
			}
			
		}
		else { return r.readEntity(type); }
	}
	
	/**
	 * Validates an object using this client's schema and context-
	 * 
	 * @param o The object to validate.
	 * @throws ResourceValidationException if validation fails.
	 */
	private void validate(Object o) throws ResourceValidationException {
		try {
			JAXBSource src = new JAXBSource(context, o);
			validator.validate(src);
		}
		catch (Exception e) { throw new ResourceValidationException(e); }
	}

}
