package it.polito.dp2.NFV.sol3.client1;

public class ResourceNotFoundException extends Exception {

	private static final long serialVersionUID = 6197434971909496123L;

	public ResourceNotFoundException(String msg) { super(msg); }
}
