var spec =

{
  "swagger": "2.0",
  "info": {
    "description": "The NfvDeployer service is a web interface toward the DP2 Nfv System. In the context of Network Function Virtualization (NFV), this service allows access to the underlying Infrastructure Network (or IN for short). Through it it is possible to read informations about the available hosts and their connections, the hosted Network Function Forwarding Graphs and their topology as well as the Virtual Network Functions offered by the IN. For more informations regarding the documentation see the design page here: <a href='design.html'>click</a>.",
    "version": "1.0.0",
    "title": "Nfv Deployer"
  },
  "basePath": "/NfvDeployer/rest",
  "tags": [
    {
      "name": "Virtual Network Function",
      "description": "One of the functionalities that the Infrastructure Network is capable of hosting. Each VNF belongs to a specific Functional Type such as Mail Server, Firewall ecc."
    },
    {
      "name": "Host",
      "description": "Servers available for hosting within the Infrastructure Network."
    },
    {
      "name": "Network Functions Forwarding Graph",
      "description": "NFFG for short. A set of nodes implementing a Virtual Network Function offered by the Infrastructure Network."
    },
    {
      "name": "Node",
      "description": "An NFFG Node. It is hosted on one of the IN and offers a specific VNF which the IN is capable of virtualizing."
    },
    {
      "name": "Link",
      "description": "An unidirectional interconnection between NFFG nodes. It is characterized by a link latency (in ms) and throughput (in Mbps)."
    }
  ],
  "schemes": [
    "http"
  ],
  "paths": {
    "/vnfs": {
      "get": {
        "tags": [
          "Virtual Network Function"
        ],
        "summary": "Retrieves the VNF Catalog offered by the Infrastructure Network",
        "description": "",
        "operationId": "getVNFCatalog",
        "produces": [
          "application/xml"
        ],
        "responses": {
          "200": {
            "description": "List of VNF objects available in the Infrastructure Network",
            "schema": {
              "$ref": "#/definitions/VNFCatalog"
            }
          }
        }
      }
    },
    "/hosts": {
      "get": {
        "tags": [
          "Host"
        ],
        "summary": "Retrieves all Hosts available in the Infrastructure Network",
        "description": "",
        "operationId": "getHosts",
        "produces": [
          "application/xml"
        ],
        "responses": {
          "200": {
            "description": "List of hyperlinks to hosts available in the Infrastructure Network",
            "schema": {
              "$ref": "#/definitions/HostRefs"
            }
          }
        }
      }
    },
    "/hosts/{hostName}": {
      "get": {
        "tags": [
          "Host"
        ],
        "summary": "Find host by name",
        "description": "",
        "operationId": "getHostByName",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "hostName",
            "description": "The requested host's name",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested host representation",
            "schema": {
              "$ref": "#/definitions/Host"
            }
          },
          "404": {
            "description": "Host not found"
          }
        }
      }
    },
    "/hosts/{hostName}/nodes": {
      "get": {
        "tags": [
          "Host"
        ],
        "summary": "Returns references to all nodes allocated on this host",
        "description": "",
        "operationId": "getAllocatedNodes",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "hostName",
            "description": "The host whose nodes are requested.",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The list of hyperlinks to all NFFG nodes allocated on this host.",
            "schema": {
              "$ref": "#/definitions/Allocations"
            }
          },
          "404": {
            "description": "Host not found"
          }
        }
      }
    },
    "/hosts/{hostName}/connections": {
      "get": {
        "tags": [
          "Host"
        ],
        "summary": "Returns connections starting from this host.",
        "description": "",
        "operationId": "getConnections",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "hostName",
            "description": "The host whose connections are requested.",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "query",
            "name": "dest",
            "description": "The destination host name. If none is specified, all connections starting from this host are returned.",
            "required": false,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The connection starting from this host and ending to the destination host, or all outgoing connection if no destination was specified. A link to the destination host is provided within the href attribute.",
            "schema": {
              "$ref": "#/definitions/Connections"
            }
          },
          "404": {
            "description": "Host not found, or no connection going from this host to the specified destination."
          }
        }
      }
    },
    "/nffgs": {
      "get": {
        "tags": [
          "Network Functions Forwarding Graph"
        ],
        "summary": "Returns NF-FGs deployed on the Infrastructure Network",
        "description": "Queries and returns hyperlinks to deployed NF-FGs since a given date, passed as parameter. If the date is not specified, all deployed NF-FGs are returned.",
        "operationId": "getNffgsByDate",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "since",
            "description": "Specifies the minimum deployment date to filter the returned NF-FGs. Format must be ISO 8601 date-time. If none is specified, all deployed NF-FGs are returned.",
            "required": false,
            "type": "string",
            "format": "date-time"
          }
        ],
        "responses": {
          "200": {
            "description": "The list of hyperlinks to deployed NFFGs since the given date, or to all deployed NFFGs if no date was specified",
            "schema": {
              "$ref": "#/definitions/NFFGRefs"
            }
          },
          "400": {
            "description": "Invalid date string."
          }
        }
      },
      "post": {
        "tags": [
          "Network Functions Forwarding Graph"
        ],
        "summary": "Deploys a new NF-FG onto the Infrastructure Network",
        "description": "Deploys the NF-FG onto the Infrastructure Network. Nodes are automatically allocated on available hosts; it is possible to suggest the desired host (through the 'host' attribute), but the system is free to decide a different one. Each node must also reference (by name) one of the VNFs available in the catalog (otherwise, the operation fails with code 400). Deployment time is set to the current time. The request fails if the system is unable to allocate all nodes.",
        "operationId": "deployNffg",
        "consumes": [
          "application/xml"
        ],
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "NFFG",
            "description": "The NFFG to deploy.",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NFFGUpload"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "NF-FG successfully deployed. Returned object is a link to the allocated NF-FG.",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/ObjectReference"
                },
                {
                  "xml": {
                    "name": "NFFGRef"
                  },
                  "example": {
                    "name": "Nffg1",
                    "href": "/nffgs/Nffg1"
                  }
                }
              ]
            }
          },
          "400": {
            "description": "Invalid NF-FG object"
          },
          "409": {
            "description": "NF-FG or node names already in use."
          },
          "507": {
            "description": "Unable to allocate all the NF-FG nodes"
          }
        }
      }
    },
    "/nffgs/{nffgName}": {
      "get": {
        "tags": [
          "Network Functions Forwarding Graph"
        ],
        "summary": "Reads information about a specific, deployed NF-FG",
        "description": "",
        "operationId": "getNffgByName",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The requested NF-FG's name",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested NF-FG.",
            "schema": {
              "$ref": "#/definitions/NFFG"
            }
          },
          "404": {
            "description": "NF-FG not found"
          }
        }
      },
      "delete": {
        "tags": [
          "Network Functions Forwarding Graph"
        ],
        "summary": "Undeploys this NF-FG from the Infrastructure Network.",
        "description": "The NF-FG specified is undeployed (thus deleted) from the Infrastructure Network. All its allocation relationships are removed too.",
        "operationId": "undeployNffg",
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The name of the NF-FG to undeploy",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "produces": [
          "text/plain"
        ],
        "responses": {
          "204": {
            "description": "NF-FG successfully undeployed"
          },
          "400": {
            "description": "Invalid NF-FG name"
          },
          "404": {
            "description": "NF-FG not found"
          }
        }
      }
    },
    "/nffgs/{nffgName}/nodes": {
      "get": {
        "tags": [
          "Node"
        ],
        "summary": "Returns the list of nodes belonging to this NF-FG",
        "description": "",
        "operationId": "getNffgNodes",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG whose node to fetch",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The list of nodes belonging to the requested NF-FG.",
            "schema": {
              "type": "object",
              "properties": {
                "Node": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/Node"
                  },
                  "xml": {
                    "name": "Node"
                  }
                }
              },
              "xml": {
                "name": "Nodes"
              },
              "example": {
                "Node": [
                  {
                    "name": "NodeA",
                    "vnf": "CACHE1",
                    "host": "H0",
                    "hostRef": "/hosts/H0",
                  },
                  {
                    "name": "NodeB",
                    "vnf": "SPAM2",
                    "host": "H1",
                    "hostRef": "/hosts/H1",
                    "links": [
                      {
                        "name": "LinkB",
                        "srcRef": "/nffgs/Nffg1/nodes/NodeB",
                        "dest": "NodeA",
                        "destRef": "/nffgs/Nffg1/nodes/NodeA",
                        "latency": 12
                      }
                    ]
                  }
                ]
              }
            }
          },
          "404": {
            "description": "NF-FG not found"
          }
        }
      },
      "post": {
        "tags": [
          "Node"
        ],
        "summary": "Adds a single node to this NF-FG.",
        "description": "Adds the posted node to this NF-FG. The node is added without any Link (if any is present, it will be deleted), and it's automatically allocated on a host. It is possible to suggest a desired host for allocation (through the 'host' attribute of each Node), but allocation on the requested host is not guaranteed. The node must reference (by name) one of the VNFs offered by the catalog, otherwise deployment fails (error 400). If the node cannot be allocated on the Infrastructure Network, the operation fails.",
        "operationId": "addNewNode",
        "consumes": [
          "application/xml"
        ],
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG to whom add the new Node.",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "body",
            "name": "node",
            "description": "The node to add.",
            "schema": {
              "$ref": "#/definitions/NodeUpload"
            },
            "required": true
          }
        ],
        "responses": {
          "201": {
            "description": "Node was correctly allocated. Returned object is a hyperlink to the allocated node.",
            "schema": {
              "allOf": [
                {
                  "$ref": "#/definitions/ObjectReference"
                },
                {
                  "xml": {
                    "name": "NodeRef"
                  }
                }
              ],
              "example": {
                "name": "NodeC",
                "href": "/nffgs/Nffg1/nodes/NodeC"
              }
            }
          },
          "400": {
            "description": "Invalid Node format"
          },
          "404": {
            "description": "NF-FG not found"
          },
          "409": {
            "description": "Node name already in use"
          },
          "507": {
            "description": "Allocation of the node failed"
          }
        }
      }
    },
    "/nffgs/{nffgName}/nodes/{nodeName}": {
      "get": {
        "tags": [
          "Node"
        ],
        "summary": "Reads information about a Node from this NF-FG",
        "description": "",
        "operationId": "getNode",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG whose node to fetch",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "nodeName",
            "description": "The requested node name",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested node",
            "schema": {
              "$ref": "#/definitions/Node"
            }
          },
          "404": {
            "description": "NF-FG or Node not found"
          }
        }
      },
      "delete": {
        "tags": [
          "Node"
        ],
        "summary": "Removes this node from the NF-FG",
        "description": "The node is deleted from its NF-FG, as well as its allocation. Removal is possible only if the node is not bound by any outgoing or ingoing link.",
        "operationId": "removeNode",
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG whose node to fetch",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "nodeName",
            "description": "The requested node name",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "produces": [
          "text/plain"
        ],
        "responses": {
          "204": {
            "description": "Node successfully removed"
          },
          "404": {
            "description": "NF-FG or Node not found"
          },
          "500": {
            "description": "Node removal failed"
          }
        }
      }
    },
    "/nffgs/{nffgName}/nodes/{nodeName}/reachableHosts": {
      "get": {
        "tags": [
          "Node"
        ],
        "summary": "Returns the list of hosts that are reachable from this node.",
        "description": "Returns the list of hyperlinks to hosts that are reachable from this node. This includes the host the node is allocated on.",
        "operationId": "getReachableHosts",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG whose node to fetch",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "nodeName",
            "description": "The requested node name",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The list of hyperlinks to hosts that are reachable from this node",
            "schema": {
              "$ref": "#/definitions/HostRefs"
            }
          },
          "404": {
            "description": "NF-FG or Node not found"
          }
        }
      }
    },
    "/nffgs/{nffgName}/nodes/{nodeName}/links": {
      "get": {
        "tags": [
          "Link"
        ],
        "summary": "Returns the list of all links outgoing from this node",
        "description": "",
        "operationId": "getLinks",
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG to whom the node to check belongs.",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "nodeName",
            "description": "The requested node name.",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "responses": {
          "200": {
            "description": "The list of links going out of this node. A hyperlink to the destination node is provided within the href attribute.",
            "schema": {
              "type": "object",
              "properties": {
                "Link": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/Link"
                  },
                  "xml": {
                    "name": "Link"
                  }
                }
              },
              "xml": {
                "name": "Links"
              },
              "example": {
                "Link": [
                  {
                    "name": "LinkA",
                    "srcRef": "/nffgs/Nffg1/nodes/NodeA",
                    "dest": "NodeB",
                    "destRef": "/nffgs/Nffg1/nodes/NodeB",
                    "latency": 13
                  },
                  {
                    "name": "LinkB",
                    "srcRef": "/nffgs/Nffg1/nodes/NodeA",
                    "dest": "NodeC",
                    "destRef": "/nffgs/Nffg1/nodes/NodeC",
                    "latency": 3
                  }
                ]
              }
            }
          },
          "404": {
            "description": "NF-FG or Node not found"
          }
        }
      },
      "post": {
        "tags": [
          "Link"
        ],
        "summary": "Creates a new node link, starting from this node.",
        "description": "The new link starts from this node and is connected to the node specified in the 'dest' attribute. If a link to that node already exists it is possible to specify whether to override it or not.",
        "operationId": "addNewLink",
        "consumes": [
          "application/xml"
        ],
        "produces": [
          "application/xml"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG of the requested node",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "nodeName",
            "description": "The source node of the posted link",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "query",
            "name": "overwrite",
            "description": "Whether to overwrite an already existing link. Default is false.",
            "type": "boolean",
            "required": false,
            "default": false
          },
          {
            "in": "body",
            "name": "link",
            "description": "The link to create.",
            "schema": {
              "$ref": "#/definitions/LinkUpload"
            },
            "required": true
          }
        ],
        "responses": {
          "201": {
            "description": "Link successfully created. Returned object is a representation of the posted link.",
            "schema": {
              "$ref": "#/definitions/Link"
            }
          },
          "400": {
            "description": "Invalid link data"
          },
          "404": {
            "description": "Nffg or node not found"
          },
          "409": {
            "description": "Already existing link, not overwritten, or existing link with a different source/destination."
          }
        }
      }
    },
    "/nffgs/{nffgName}/nodes/{nodeName}/links/{linkName}": {
      "delete": {
        "tags": [
          "Link"
        ],
        "summary": "Deletes this link.",
        "description": "",
        "operationId": "deleteLink",
        "parameters": [
          {
            "in": "path",
            "name": "nffgName",
            "description": "The NF-FG of the requested node",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "nodeName",
            "description": "The source node of the link to delete",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          },
          {
            "in": "path",
            "name": "linkName",
            "description": "The name of the link to delete",
            "required": true,
            "type": "string",
            "pattern": "[A-Za-z][a-zA-Z0-9]*"
          }
        ],
        "produces": [
          "text/plain"
        ],
        "responses": {
          "204": {
            "description": "Link successfully deleted"
          },
          "404": {
            "description": "Nffg, Node or Link not found"
          }
        }
      }
    }
  },
  "definitions": {
    "VNFCatalog": {
      "type": "object",
      "properties": {
        "VNF": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/VNF"
          },
          "example": [
            {
              "name": "CACHE1",
              "type": "CACHE",
              "memoryMB": 150,
              "storageMB": 250
            },
            {
              "name": "VPN2",
              "type": "VPN",
              "memoryMB": 300,
              "storageMB": 340
            }
          ]
        }
      },
      "required": [
        "VNF"
      ],
      "xml": {
        "name": "VNFCatalog"
      }
    },
    "VNF": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "type": {
          "$ref": "#/definitions/FunctionalType"
        },
        "memoryMB": {
          "type": "int",
          "xml": {
            "attribute": true
          }
        },
        "storageMB": {
          "type": "int",
          "xml": {
            "attribute": true
          }
        }
      },
      "required": [
        "name",
        "type",
        "memoryMB",
        "storageMB"
      ],
      "example": {
        "name": "CACHE1",
        "type": "CACHE",
        "memoryMB": 100,
        "storageMB": 250
      }
    },
    "HostRefs": {
      "type": "object",
      "properties": {
        "HostRef": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ObjectReference"
          },
          "example": [
            {
              "name": "H0",
              "href": "/hosts/H0"
            },
            {
              "name": "H1",
              "href": "/hosts/H1"
            }
          ]
        }
      }
    },
    "Host": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "maxVNF": {
          "$ref": "#/definitions/PositiveInt32"
        },
        "memoryMB": {
          "$ref": "#/definitions/PositiveInt32"
        },
        "storageMB": {
          "$ref": "#/definitions/PositiveInt32"
        },
        "Connections": {
          "$ref": "#/definitions/Connections"
        },
        "Allocations": {
          "$ref": "#/definitions/Allocations"
        }
      },
      "required": [
        "name",
        "maxVNF",
        "memoryMB",
        "storageMB",
        "Connections",
        "Allocations"
      ],
      "example": {
        "name": "H0",
        "maxVNF": 10,
        "memoryMB": 800,
        "storageMB": 4096
      }
    },
    "Connections": {
      "type": "object",
      "properties": {
        "Connection": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Connection"
          }
        }
      }
    },
    "Connection": {
      "type": "object",
      "properties": {
        "to": {
          "$ref": "#/definitions/EntityName"
        },
        "latency": {
          "$ref": "#/definitions/PositiveInt32"
        },
        "throughput": {
          "$ref": "#/definitions/CPThroughput"
        },
        "href": {
          "$ref": "#/definitions/uri"
        }
      },
      "required": [
        "to",
        "latency",
        "throughput",
        "href"
      ],
      "example": {
        "to": "H1",
        "href": "/hosts/H1",
        "latency": 10
      }
    },
    "Allocations": {
      "type": "object",
      "properties": {
        "NodeAllocation": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/NodeAllocation"
          },
          "example": [
            {
              "nffg": "Nffg1",
              "node": "NodeA",
              "href": "/nffgs/Nffg1/NodeA"
            },
            {
              "nffg": "Nffg2",
              "node": "NodeB",
              "href": "/nffgs/Nffg2/NodeB"
            }
          ]
        }
      }
    },
    "NodeAllocation": {
      "type": "object",
      "properties": {
        "nffg": {
          "$ref": "#/definitions/EntityName"
        },
        "node": {
          "$ref": "#/definitions/EntityName"
        },
        "href": {
          "$ref": "#/definitions/uri"
        }
      },
      "required": [
        "nffg",
        "node",
        "href"
      ],
      "example": {
        "nffg": "Nffg1",
        "node": "NodeA",
        "href": "/nffgs/Nffg1/NodeA"
      }
    },
    "NFFGRefs": {
      "type": "object",
      "properties": {
        "NFFGRef": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ObjectReference"
          },
          "example": [
            {
              "name": "Nffg1",
              "href": "/nffgs/Nffg1"
            },
            {
              "name": "Nffg2",
              "href": "/nffgs/Nffg2"
            }
          ]
        }
      }
    },
    "NFFGUpload": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "Node": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/NodeUpload"
          }
        }
      },
      "required": [
        "name"
      ],
      "xml": {
        "name": "NFFG"
      },
      "example": {
        "name": "Nffg1",
        "Node": [
          {
            "name": "NodeA",
            "vnf": "CACHE1",
            "host": "H0"
          },
          {
            "name": "NodeB",
            "vnf": "SPAM1",
            "host": "H1"
          }
        ]
      }
    },
    "NFFG": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "deployTime": {
          "type": "string",
          "format": "date-time",
          "xml": {
            "attribute": true
          }
        },
        "Node": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Node"
          }
        }
      },
      "required": [
        "name",
        "deployTime"
      ],
      "example": {
        "name": "Nffg1",
        "Node": [
          {
            "name": "NodeA",
            "vnf": "MAIL_CLIENT1",
            "host": "H0",
            "hostRef": "/hosts/H0",
            "nffg": "Nffg1"
          },
          {
            "name": "NodeB",
            "vnf": "SPAM2",
            "host": "H2",
            "hostRef": "/hosts/H2",
            "nffg": "Nffg1",
            "links": [
              {
                "name": "LinkB",
                "srcRef": "/nffgs/Nffg1/nodes/NodeB",
                "dest": "NodeA",
                "dstRef": "/nffgs/Nffg1/nodes/NodeA",
                "latency": 10,
                "throughput": 13.4
              }
            ]
          }
        ]
      }
    },
    "NodeUpload": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "vnf": {
          "$ref": "#/definitions/EntityName"
        },
        "host": {
          "$ref": "#/definitions/EntityName"
        }
      },
      "required": [
        "name",
        "vnf"
      ],
      "xml": {
        "name": "Node"
      },
      "example": {
        "name": "NodeA",
        "vnf": "CACHE1",
        "host": "H0"
      }
    },
    "Node": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "vnf": {
          "$ref": "#/definitions/EntityName"
        },
        "host": {
          "$ref": "#/definitions/EntityName"
        },
        "hostRef": {
          "$ref": "#/definitions/uri"
        },
        "links": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Link"
          }
        }
      },
      "xml": {
        "name": "Node"
      },
      "required": [
        "name",
        "vnf",
        "host",
        "links"
      ],
      "example": {
        "name": "Nffg1NodeA",
        "vnf": "MAIL_CLIENT1",
        "host": "H1",
        "hostRef": "/hosts/H1"
      }
    },
    "LinkUpload": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "dest": {
          "$ref": "#/definitions/EntityName"
        },
        "latency": {
          "allOf": [
            {
              "$ref": "#/definitions/PositiveInt32"
            },
            {
              "default": 0
            }
          ]
        },
        "throughput": {
          "type": "number",
          "format": "float",
          "minimum": 0,
          "xml": {
            "attribute": true
          },
          "example": 12.3
        }
      },
      "required": [
        "name",
        "dest"
      ],
      "xml": {
        "name": "Link"
      },
      "example": {
        "name": "LinkA",
        "dest": "NodeB",
        "latency": 15
      }
    },
    "Link": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "srcRef": {
          "$ref": "#/definitions/uri"
        },
        "dest": {
          "$ref": "#/definitions/EntityName"
        },
        "destRef": {
          "$ref": "#/definitions/uri"
        },
        "latency": {
          "allOf": [
            {
              "$ref": "#/definitions/PositiveInt32"
            },
            {
              "default": 0
            }
          ]
        },
        "throughput": {
          "type": "number",
          "format": "float",
          "minimum": 0,
          "xml": {
            "attribute": true
          },
          "example": 12.3
        }
      },
      "required": [
        "name",
        "srcRef",
        "dest",
        "destRef"
      ],
      "xml": {
        "name": "Link"
      },
      "example": {
        "name": "LinkA",
        "srcRef": "/nffgs/Nffg1/nodes/NodeA",
        "dest": "NodeB",
        "destRef": "/nffgs/Nffg1/nodes/NodeB",
        "latency": 15
      }
    },
    "EntityName": {
      "type": "string",
      "pattern": "[A-Za-z][0-9A-Za-z]*",
      "xml": {
        "attribute": true
      }
    },
    "FunctionalType": {
      "type": "string",
      "enum": [
        "CACHE",
        "DPI",
        "FW",
        "MAIL_CLIENT",
        "MAIL_SERVER",
        "NAT",
        "SPAM",
        "VPN",
        "WEB_CLIENT",
        "WEB_SERVER"
      ],
      "xml": {
        "attribute": true
      }
    },
    "ObjectReference": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/EntityName"
        },
        "href": {
          "$ref": "#/definitions/uri"
        }
      },
      "required": [
        "name",
        "href"
      ]
    },
    "uri": {
      "type": "string",
      "format": "uri",
      "xml": {
        "attribute": true
      }
    },
    "PositiveInt32": {
      "type": "integer",
      "format": "int32",
      "minimum": 0,
      "xml": {
        "attribute": true
      }
    },
    "CPThroughput": {
      "type": "number",
      "format": "float",
      "pattern": "\\+?\\d*\\.\\d+",
      "example": 1.124,
      "xml": {
        "attribute": true
      }
    }
  }
}