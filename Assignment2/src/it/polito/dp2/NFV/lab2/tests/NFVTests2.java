package it.polito.dp2.NFV.lab2.tests;

import org.junit.BeforeClass;
import org.junit.Test;

import it.polito.dp2.NFV.lab2.ReachabilityTester;
import it.polito.dp2.NFV.lab2.ServiceException;
import it.polito.dp2.NFV.sol2.ReachabilityTesterFactory;

public class NFVTests2 {
	
	private static ReachabilityTester tester; 
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("it.polito.dp2.NFV.lab2.URL", "http://localhost:8080/Neo4JSimpleXML/webapi");
		System.setProperty("it.polito.dp2.NFV.NfvReaderFactory", "it.polito.dp2.NFV.Random.NfvReaderFactoryImpl");
		System.setProperty("it.polito.dp2.NFV.lab2.ReachabilityTesterFactory", "it.polito.dp2.NFV.sol2.ReachabilityTesterFactory");
		
		tester = ReachabilityTesterFactory.newInstance().newReachabilityTester();
	}

	@Test(expected = ServiceException.class)
	public final void TestNoNeo4J() throws Exception {
		tester.loadGraph("Nffg0");
	}
}
