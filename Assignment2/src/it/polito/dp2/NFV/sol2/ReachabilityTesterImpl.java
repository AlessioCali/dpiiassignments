package it.polito.dp2.NFV.sol2;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import it.polito.dp2.NFV.FactoryConfigurationError;
import it.polito.dp2.NFV.HostReader;
import it.polito.dp2.NFV.LinkReader;
import it.polito.dp2.NFV.NffgReader;
import it.polito.dp2.NFV.NfvReader;
import it.polito.dp2.NFV.NfvReaderException;
import it.polito.dp2.NFV.NfvReaderFactory;
import it.polito.dp2.NFV.NodeReader;
import it.polito.dp2.NFV.VNFTypeReader;
import it.polito.dp2.NFV.lab2.AlreadyLoadedException;
import it.polito.dp2.NFV.lab2.ExtendedNodeReader;
import it.polito.dp2.NFV.lab2.NoGraphException;
import it.polito.dp2.NFV.lab2.ReachabilityTester;
import it.polito.dp2.NFV.lab2.ServiceException;
import it.polito.dp2.NFV.lab2.UnknownNameException;

class ReachabilityTesterImpl implements ReachabilityTester {
	
	private static final String N4J_SCHEMA_LOC = "custom/Neo4JSimpleXML.xsd";
	
	private NfvReader source;
	private Client client;
	private WebTarget target;
	private Validator validator;
	private JAXBContext context;
	
	// A map that holds, for each NFFG name, the mapping between the NFFG nodes and N4J Node Ids
	private HashMap<String, HashMap<String, String>> nffgNodeIds = new HashMap<>();
	
	// A map that holds each N4J Node Id for loaded hosts
	private HashMap<String, String> hostIds = new HashMap<>();
	
	private class N4JExtendedNodeReader implements ExtendedNodeReader {
		
		private NodeReader src;
		private WebTarget target;
		private NfvReader nfv;
		
		protected N4JExtendedNodeReader(NodeReader src, WebTarget target, NfvReader nfv) {
			this.src = src;
			this.target = target;
			this.nfv = nfv;
		}

		@Override
		public VNFTypeReader getFuncType() {
			return src.getFuncType();
		}

		@Override
		public HostReader getHost() {
			return src.getHost();
		}

		@Override
		public Set<LinkReader> getLinks() {
			return src.getLinks();
		}

		@Override
		public NffgReader getNffg() {
			return src.getNffg();
		}

		@Override
		public String getName() {
			return src.getName();
		}

		@Override
		public Set<HostReader> getReachableHosts() throws NoGraphException, ServiceException {
			try {
				Nodes ns = target.path("reachableNodes")
								 .queryParam("nodeLabel", "Host")
								 .request(MediaType.APPLICATION_XML_TYPE)
								 .get(Nodes.class);
				
				try {
					JAXBSource nodeSource = new JAXBSource(context, ns);
					validator.validate(nodeSource);
				}
				catch (JAXBException je) { throw new ServiceException("Server data could not be deserialized"); }
				catch (SAXException se) { throw new ServiceException("Server data is invalid with respect to schema"); }
				catch (IOException ioe) { throw new ServiceException("An error occurred while parsing server data"); }

				return ns.getNode().stream()
								   // Extract not-null properties named "name"
								   .map(n -> 
								   		n.getProperties().getProperty().stream()
								   									   .filter(p -> p.getName().equals("name"))
										   							   .findFirst()
										   							   .orElse(null)
					 			   )
								   .filter(p -> p != null)
								   // Collect matching host readers
								   .map(p -> nfv.getHost(p.getValue()))
								   .collect(Collectors.toSet());
			}
			catch (Exception e) {
				throw new ServiceException("An unexpected error occurred: ", e);
			}
		}

	}
	
	protected ReachabilityTesterImpl(URI baseURI) throws NfvReaderException, FactoryConfigurationError {
		NfvReaderFactory nfvf = NfvReaderFactory.newInstance();
		source = nfvf.newNfvReader();
		client = ClientBuilder.newClient();
		target = client.target(baseURI);
		
		try {
			context = JAXBContext.newInstance("it.polito.dp2.NFV.sol2");
			BufferedInputStream schemaInput = new BufferedInputStream(new FileInputStream(N4J_SCHEMA_LOC));	
			
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(new StreamSource(schemaInput));
			
			validator = schema.newValidator();
		}
		catch (JAXBException je) { throw new NfvReaderException("Could not instantiate JAXB Context"); }
		catch (FileNotFoundException fnf) { throw new NfvReaderException("Could not locate Neo4J Schema"); } 
		catch (SAXException e) { throw new NfvReaderException("Error while creating Schema"); }		
	}
	
	@Override
	public void loadGraph(String nffgName) throws UnknownNameException, AlreadyLoadedException, ServiceException {
		if (isLoaded(nffgName)) { throw new AlreadyLoadedException("NFFG " + nffgName + " is already loaded."); }
		
		try {
			HashMap<String, String> nodeIds = new HashMap<>();
			nffgNodeIds.put(nffgName, nodeIds);
			Set<NodeReader> nffgNodes = source.getNffg(nffgName).getNodes();
			
			// Load each node in the NFFG and eventually their Hosts. Also link nodes to hosts.
			for (NodeReader nr : nffgNodes) {
				HostReader hr = nr.getHost();
				String nffgNodeId, hostNodeId;
				nffgNodeId = postNffgNode(nr, nodeIds);
				if (!hostIds.containsKey(hr.getName())) { hostNodeId = postHost(hr); }
				else { hostNodeId = hostIds.get(hr.getName()); }
				allocatedOn(nffgNodeId, hostNodeId);
			}
			
			// Once all nodes are loaded onto the system, post forwardsTo relationships
			for (NodeReader nr : nffgNodes) {
				String sourceId = nodeIds.get(nr.getName());
				
				// Exceptional case, should not happen
				if (sourceId == null) { 
					throw new ServiceException("Node " + nr.getName() + " was loaded but its Id was not found"); 
				}
				
				for (LinkReader lr : nr.getLinks()) {
					NodeReader dest = lr.getDestinationNode();
					String destId;
					
					// Exceptional case, should not happen
					if (dest == null || (destId = nodeIds.get(dest.getName())) == null) {
						throw new ServiceException("Node " + nr.getName() + " forwards to an invalid node");
					}
					
					forwardsTo(sourceId, destId);
				}
			}
		}
		catch (Exception e) {
			// Clean local state
			nffgNodeIds.remove(nffgName);
			throw new ServiceException("An unexpected error occurred", e);
		}
	}

	/**
	 * Posts a "forwardTo" relationship.
	 * 
	 * @param sourceId source NFFG node id
	 * @param destId destination NFFG node id
	 */
	private void forwardsTo(String sourceId, String destId) {
		Relationship rel = new Relationship();
		rel.setDstNode(destId);
		rel.setType("ForwardsTo");
		
		target.path("node")
			  .path(sourceId)
			  .path("relationships")
			  .request()
			  .post(Entity.entity(rel, MediaType.APPLICATION_XML_TYPE), Relationship.class);
	}

	/**
	 * Posts a "allocatedOn" relationship
	 * 
	 * @param nffgNodeId hosted NFFG node id
	 * @param hostNodeId hosting Host node id
	 */
	private void allocatedOn(String nffgNodeId, String hostNodeId) {
		Relationship rel = new Relationship();
		rel.setDstNode(hostNodeId);
		rel.setType("AllocatedOn");
		
		target.path("node")
			  .path(nffgNodeId)
			  .path("relationships")
			  .request()
			  .post(Entity.entity(rel, MediaType.APPLICATION_XML_TYPE), Relationship.class);
	}

	/**
	 * Posts a new Host node and save its ID
	 * 
	 * @param hr the source host reader
	 * @return the N4J node id for the posted node
	 * @throws JAXBException If it is not possible to parse the server response
	 * @throws IOException If the Neo4J Schema reader throws an IOException
	 * @throws SAXException If validation of server data fails
	 */
	private String postHost(HostReader hr) throws JAXBException, SAXException, IOException {
		Node n4jNode = new Node();
		Labels ls = new Labels();
		Properties ps = new Properties();
		Property p = new Property();
		
		ps.getProperty().add(p);
		p.name = "name";
		p.value = hr.getName();
		
		n4jNode.setProperties(ps);
		
		// Post Host node
		Node response = target.path("node")
							  .request(MediaType.APPLICATION_XML_TYPE)
							  .post(Entity.entity(n4jNode, MediaType.APPLICATION_XML_TYPE), Node.class);
		
		JAXBSource nodeSource = new JAXBSource(context, response);
		validator.validate(nodeSource);
		
		// Post Host label
		ls.getLabel().add("Host");
		target.path("node")
			  .path(response.getId())
			  .path("labels")
			  .request(MediaType.APPLICATION_XML_TYPE)
			  .post(Entity.entity(ls, MediaType.APPLICATION_XML_TYPE), String.class);
		
		hostIds.put(hr.getName(), response.getId());
		return response.getId();
	}

	/**
	 * Post a new NFFG Node node and save its id.
	 * 
	 * @param nr the source node reader
	 * @param idsMap the map where to save the id.
	 * @return the N4J node id for the posted node
	 * @throws JAXBException If it is not possible to parse the server response
	 * @throws IOException If the Neo4J Schema reader throws an IOException
	 * @throws SAXException If validation of server data fails
	 */
	private String postNffgNode(NodeReader nr, HashMap<String, String> idsMap) throws JAXBException, SAXException, IOException {
		Node n4jNode = new Node();
		Labels ls = new Labels();
		Properties ps = new Properties();
		Property p = new Property();
		
		ps.getProperty().add(p);
		p.name = "name";
		p.value = nr.getName();
		n4jNode.setProperties(ps);
		
		// Post NFFG Node
		Node response = target.path("node")
							  .request(MediaType.APPLICATION_XML_TYPE)
							  .post(Entity.entity(n4jNode, MediaType.APPLICATION_XML_TYPE), Node.class);
		
		JAXBSource nodeSource = new JAXBSource(context, response);
		validator.validate(nodeSource);
		
		// Post Node label
		ls.getLabel().add("Node");
		target.path("node")
			  .path(response.getId())
			  .path("labels")
			  .request(MediaType.APPLICATION_XML_TYPE)
			  .post(Entity.entity(ls, MediaType.APPLICATION_XML_TYPE), String.class);
		
		idsMap.put(nr.getName(), response.getId());
		return response.getId();
	}

	@Override
	public Set<ExtendedNodeReader> getExtendedNodes(String nffgName) throws UnknownNameException, NoGraphException, ServiceException {
		if (!isLoaded(nffgName)) { throw new NoGraphException("Graph with name " + nffgName + " is not loaded."); }
		HashMap<String, String> nodeIds = nffgNodeIds.get(nffgName);
		WebTarget nodeTarget = target.path("node");
				
		return source.getNffg(nffgName).getNodes().stream()
												  .map(nr -> new N4JExtendedNodeReader(nr, nodeTarget.path(nodeIds.get(nr.getName())), source))
												  .collect(Collectors.toSet());
	}

	@Override
	public boolean isLoaded(String nffgName) throws UnknownNameException {
		if (nffgName == null) { throw new UnknownNameException("NFFG name is null"); }
		if (source.getNffg(nffgName) == null) { throw new UnknownNameException("No NFFG with name " + nffgName + " found"); }
		return nffgNodeIds.containsKey(nffgName);
	}

}
