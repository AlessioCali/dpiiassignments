package it.polito.dp2.NFV.sol2;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriBuilderException;

import it.polito.dp2.NFV.FactoryConfigurationError;
import it.polito.dp2.NFV.NfvReaderException;
import it.polito.dp2.NFV.lab2.ReachabilityTester;
import it.polito.dp2.NFV.lab2.ReachabilityTesterException;

public class ReachabilityTesterFactory extends it.polito.dp2.NFV.lab2.ReachabilityTesterFactory {

	@Override
	public ReachabilityTester newReachabilityTester() throws ReachabilityTesterException {
		try {
			URI uri = getBaseURI();
			ReachabilityTesterImpl tester = new ReachabilityTesterImpl(uri);
			return tester;
		}
		catch (FactoryConfigurationError fce) {
			throw new ReachabilityTesterException("Error in factory configuration");
		}
		catch (NfvReaderException nfve) {
			throw new ReachabilityTesterException(nfve, "Error creating NfvReader.");
		}
		catch (Exception e) {
			throw new ReachabilityTesterException(e, "Unexpected exception");
		}
	}

	public static URI getBaseURI() throws ReachabilityTesterException {
		String url = System.getProperty("it.polito.dp2.NFV.lab2.URL");
		if (url == null) { 
			throw new ReachabilityTesterException("No target URL was set.");
		}
		
		try {
			return UriBuilder.fromUri(url).path("data").build();
		}
		catch (IllegalArgumentException | UriBuilderException e) {
			throw new ReachabilityTesterException(e, "Invalid URI arguments");
		}
	}
}
